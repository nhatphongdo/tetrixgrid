//
//  UserData.swift
//  TetrixGrid
//
//  Created by Do Nhat Phong on 1/26/15.
//  Copyright (c) 2015 VietDev Corporation. All rights reserved.
//

import Foundation
import UIKit
import SpriteKit
import GameKit

class UserData: NSObject {
    
    struct UserSettings {
        static var gameCenterEnabled: Bool = false
        static var userName: String = ""
        static var leaderboardIdentifier: String = ""
        static var localPlayer: GKLocalPlayer? = nil
        static var avatar: SKTexture? = nil
        static var soundEnabled: Bool = true
        static var musicEnabled: Bool = true
        static var bestScore: Int = 0
        static var gameCenterAchievements = [String:GKAchievement]()
        static var gameCenter: GameCenter? = nil
    }
    
    class func loadUserSettings() {
        UserSettings.soundEnabled = loadUserData("SoundEnabled", defaultValue: true) as Bool
        UserSettings.musicEnabled = loadUserData("MusicEnabled", defaultValue: true) as Bool
        UserSettings.bestScore = loadUserData("BestScore", defaultValue: 0) as Int
    }
    
    class func loadUserData(name: String, defaultValue: AnyObject) -> AnyObject {
        if let result: AnyObject = NSUserDefaults.standardUserDefaults().objectForKey(name) {
            return result
        }
        else {
            return defaultValue
        }
    }
    
    class func setUserData(name: String, value: Int) {
        NSUserDefaults.standardUserDefaults().setInteger(value, forKey: name)
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
    class func setUserData(name: String, value: String) {
        NSUserDefaults.standardUserDefaults().setObject(value, forKey: name)
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
    class func setUserData(name: String, value: Float) {
        NSUserDefaults.standardUserDefaults().setFloat(value, forKey: name)
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
    class func setUserData(name: String, value: Bool) {
        NSUserDefaults.standardUserDefaults().setBool(value, forKey: name)
        NSUserDefaults.standardUserDefaults().synchronize()
    }
    
}