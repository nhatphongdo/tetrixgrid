//
//  SoundLib.swift
//  TetrixGrid
//
//  Created by Do Nhat Phong on 1/26/15.
//  Copyright (c) 2015 VietDev Corporation. All rights reserved.
//

import Foundation
import AVFoundation
import SpriteKit

enum SoundNames {
    case FailedDrop
    case Touch
    case Loop01
    case Loop02
    case Loop03
    case Loop04
    case Clear
    case Lose
    case NextBlockAppear
    case BlockAppear
    case Drop
}

class SoundLib : NSObject {
    struct SoundAssets {
        static let FailedDrop = AVAudioPlayer(contentsOfURL: NSBundle.mainBundle().URLForResource("FailedDrop", withExtension: "wav"), error: nil)
        static let Touch = AVAudioPlayer(contentsOfURL: NSBundle.mainBundle().URLForResource("Touch", withExtension: "wav"), error: nil)
        static let Loop01 = AVAudioPlayer(contentsOfURL: NSBundle.mainBundle().URLForResource("Loop01", withExtension: "wav"), error: nil)
        static let Loop02 = AVAudioPlayer(contentsOfURL: NSBundle.mainBundle().URLForResource("Loop02", withExtension: "wav"), error: nil)
        static let Loop03 = AVAudioPlayer(contentsOfURL: NSBundle.mainBundle().URLForResource("Loop03", withExtension: "wav"), error: nil)
        static let Loop04 = AVAudioPlayer(contentsOfURL: NSBundle.mainBundle().URLForResource("Loop04", withExtension: "wav"), error: nil)
        static let Clear = AVAudioPlayer(contentsOfURL: NSBundle.mainBundle().URLForResource("Flutter By", withExtension: "wav"), error: nil)
        static let Lose = AVAudioPlayer(contentsOfURL: NSBundle.mainBundle().URLForResource("Lose", withExtension: "wav"), error: nil)
        static let NextBlockAppear = AVAudioPlayer(contentsOfURL: NSBundle.mainBundle().URLForResource("NextBlockAppear", withExtension: "wav"), error: nil)
        static let BlockAppear = AVAudioPlayer(contentsOfURL: NSBundle.mainBundle().URLForResource("BlockAppear", withExtension: "wav"), error: nil)
        static let Drop = AVAudioPlayer(contentsOfURL: NSBundle.mainBundle().URLForResource("Drop", withExtension: "wav"), error: nil)
        
        static var currentBackgroundPlayer: AVAudioPlayer = Loop01
    }
    
    class func getSoundAction(name: SoundNames) -> SKAction {
        switch name {
        case .FailedDrop:
            return SKAction.sequence([SKAction.runBlock({ () -> Void in
                SoundAssets.FailedDrop.play()
                return
            }), SKAction.waitForDuration(SoundAssets.FailedDrop.duration)])
        case .Touch:
            return SKAction.sequence([SKAction.runBlock({ () -> Void in
                SoundAssets.Touch.play()
                return
            }), SKAction.waitForDuration(SoundAssets.Touch.duration)])
        case .Loop01:
            return SKAction.sequence([SKAction.runBlock({ () -> Void in
                SoundAssets.Loop01.play()
                return
            }), SKAction.waitForDuration(SoundAssets.Loop01.duration)])
        case .Loop02:
            return SKAction.sequence([SKAction.runBlock({ () -> Void in
                SoundAssets.Loop02.play()
                return
            }), SKAction.waitForDuration(SoundAssets.Loop02.duration)])
        case .Loop03:
            return SKAction.sequence([SKAction.runBlock({ () -> Void in
                SoundAssets.Loop03.play()
                return
            }), SKAction.waitForDuration(SoundAssets.Loop03.duration)])
        case .Loop04:
            return SKAction.sequence([SKAction.runBlock({ () -> Void in
                SoundAssets.Loop04.play()
                return
            }), SKAction.waitForDuration(SoundAssets.Loop04.duration)])
        case .Clear:
            return SKAction.sequence([SKAction.runBlock({ () -> Void in
                SoundAssets.Clear.play()
                return
            }), SKAction.waitForDuration(SoundAssets.Clear.duration)])
        case .Lose:
            return SKAction.sequence([SKAction.runBlock({ () -> Void in
                SoundAssets.Lose.play()
                return
            }), SKAction.waitForDuration(SoundAssets.Lose.duration)])
        case .NextBlockAppear:
            return SKAction.sequence([SKAction.runBlock({ () -> Void in
                SoundAssets.NextBlockAppear.play()
                return
            }), SKAction.waitForDuration(SoundAssets.NextBlockAppear.duration)])
        case .BlockAppear:
            return SKAction.sequence([SKAction.runBlock({ () -> Void in
                SoundAssets.BlockAppear.play()
                return
            }), SKAction.waitForDuration(SoundAssets.BlockAppear.duration)])
        case .Drop:
            return SKAction.sequence([SKAction.runBlock({ () -> Void in
                SoundAssets.Drop.play()
                return
            }), SKAction.waitForDuration(SoundAssets.Drop.duration)])
        default:
            return SKAction()
        }
    }
    
    class func playBackground(node: SKNode) {
        let index = arc4random_uniform(400) % 4
        switch index {
        case 0:
            SoundAssets.currentBackgroundPlayer = SoundAssets.Loop01
        case 1:
            SoundAssets.currentBackgroundPlayer = SoundAssets.Loop02
        case 2:
            SoundAssets.currentBackgroundPlayer = SoundAssets.Loop03
        default:
            SoundAssets.currentBackgroundPlayer = SoundAssets.Loop04
        }
        
        let action = SKAction.sequence([SKAction.runBlock({ () -> Void in
            SoundAssets.currentBackgroundPlayer.play()
            return
        }), SKAction.waitForDuration(SoundAssets.currentBackgroundPlayer.duration + 5),
            SKAction.runBlock({ () -> Void in
                self.playBackground(node)
            })])

        node.runAction(action, withKey: "BackgroundMusic")
    }
    
    class func stopBackground(node: SKNode) {
        SoundAssets.currentBackgroundPlayer.stop()
        node.removeActionForKey("BackgroundMusic")
    }
}