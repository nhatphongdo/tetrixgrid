//
//  GuideScene.swift
//  TetrixGrid
//
//  Created by Do Nhat Phong on 2/10/15.
//  Copyright (c) 2015 VietDev Corporation. All rights reserved.
//

import SpriteKit

class GuideScene: SKScene {
    
    var viewController: MenuViewController? = nil
    
    var spriteBackground = SKSpriteNode(imageNamed: "Background")
    var spriteBackButton = SKSpriteNode(imageNamed: "Button_Quit")
    var spriteCopyrightText = SKSpriteNode(imageNamed: "Text_Copyright")
    var spriteTitle = SKSpriteNode(imageNamed: "Title")
    var spriteGuideBackground = SKSpriteNode(imageNamed: "Background_Guide")
    var spritePrevButton = SKSpriteNode(imageNamed: "Button_Prev")
    var spriteNextButton = SKSpriteNode(imageNamed: "Button_Next")
    var spriteStep01 = SKSpriteNode(imageNamed: "Guide_DragDrop")
    var spriteStep02 = SKSpriteNode(imageNamed: "Guide_Break")
    var spriteStep03 = SKSpriteNode(imageNamed: "Guide_Brick")
    var spriteStep04 = SKSpriteNode(imageNamed: "Guide_FillIn")
    var spriteStep05 = SKSpriteNode(imageNamed: "Guide_Rotate")
    var spriteStep06 = SKSpriteNode(imageNamed: "Guide_Bomb")
    var bestScoreLabel = SKLabelNode(fontNamed: "ChalkboardSE-Bold")
    
    var previousScene: SKScene? = nil
    
    var currentZoomingButton: SKSpriteNode? = nil
    
    var currentStep = 1
    
    var loaded = false
    
    override func didMoveToView(view: SKView) {
        /* Setup your scene here */
        
        if loaded {
            // Reset
            currentStep = 1
            spriteStep01.position = CGPoint(x: 0, y: 0)
            spriteStep02.position = CGPoint(x: size.width / 2, y: spriteGuideBackground.size.height / 2 - 120)
            spriteStep03.position = CGPoint(x: size.width / 2, y: spriteGuideBackground.size.height / 2 - 120)
            spriteStep04.position = CGPoint(x: size.width / 2, y: spriteGuideBackground.size.height / 2 - 120)
            spriteStep05.position = CGPoint(x: size.width / 2, y: spriteGuideBackground.size.height / 2 - 120)
            spriteStep06.position = CGPoint(x: size.width / 2, y: spriteGuideBackground.size.height / 2 - 120)
            spriteStep01.alpha = 1
            spriteStep02.alpha = 0
            spriteStep03.alpha = 0
            spriteStep04.alpha = 0
            spriteStep05.alpha = 0
            spriteStep06.alpha = 0
            
            return
        }
        
        let gap = CGFloat(20)
        
        spriteBackground.position = CGPoint(x: size.width / 2, y: size.height / 2)
        self.addChild(spriteBackground)
        
        // Add title
        spriteTitle.position = CGPoint(x: size.width / 2, y: size.height - spriteTitle.size.height / 2 - gap)
        self.addChild(spriteTitle)
        
        // Add copyright text
        spriteCopyrightText.position = CGPoint(x: size.width / 2, y: gap + spriteCopyrightText.size.height / 2)
        self.addChild(spriteCopyrightText)
        
        // Add exit button
        spriteBackButton.position = CGPoint(x: gap + spriteBackButton.size.width / 2, y: gap + spriteBackButton.size.height / 2)
        self.addChild(spriteBackButton)
        
        // Add guide background
        spriteGuideBackground.position = CGPoint(x: size.width / 2, y: size.height / 2)
        self.addChild(spriteGuideBackground)
        
        // Add prev button
        spritePrevButton.position = CGPoint(x: -spriteGuideBackground.size.width / 2 + spritePrevButton.size.width + gap, y: 0)
        spriteGuideBackground.addChild(spritePrevButton)
        
        // Add next button
        spriteNextButton.position = CGPoint(x: spriteGuideBackground.size.width / 2 - spriteNextButton.size.width - gap, y: 0)
        spriteGuideBackground.addChild(spriteNextButton)

        // Step 1
        spriteStep01.anchorPoint = CGPoint(x: 0.5, y: 1)
        spriteStep01.position = CGPoint(x: 0, y: spriteGuideBackground.size.height / 2 - 120)
        spriteStep01.alpha = 1
        spriteGuideBackground.addChild(spriteStep01)

        // Step 2
        spriteStep02.anchorPoint = CGPoint(x: 0.5, y: 1)
        spriteStep02.position = CGPoint(x: size.width, y: spriteGuideBackground.size.height / 2 - 120)
        spriteStep02.alpha = 0
        spriteGuideBackground.addChild(spriteStep02)

        // Step 3
        spriteStep03.anchorPoint = CGPoint(x: 0.5, y: 1)
        spriteStep03.position = CGPoint(x: size.width, y: spriteGuideBackground.size.height / 2 - 120)
        spriteStep03.alpha = 0
        spriteGuideBackground.addChild(spriteStep03)

        // Step 4
        spriteStep04.anchorPoint = CGPoint(x: 0.5, y: 1)
        spriteStep04.position = CGPoint(x: size.width, y: spriteGuideBackground.size.height / 2 - 120)
        spriteStep04.alpha = 0
        spriteGuideBackground.addChild(spriteStep04)

        // Step 5
        spriteStep05.anchorPoint = CGPoint(x: 0.5, y: 1)
        spriteStep05.position = CGPoint(x: size.width, y: spriteGuideBackground.size.height / 2 - 120)
        spriteStep05.alpha = 0
        spriteGuideBackground.addChild(spriteStep05)

        // Step 6
        spriteStep06.anchorPoint = CGPoint(x: 0.5, y: 1)
        spriteStep06.position = CGPoint(x: size.width, y: spriteGuideBackground.size.height / 2 - 120)
        spriteStep06.alpha = 0
        spriteGuideBackground.addChild(spriteStep06)

        loaded = true
    }
    
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        /* Called when a touch begins */
        
        for touch: AnyObject in touches {
            let location = touch.locationInNode(self)
            
            currentZoomingButton = nil
            let scale = SKAction.scaleTo(CGFloat(1.2), duration: 0.2)
            var action: SKAction = scale
            if UserData.UserSettings.soundEnabled {
                action = SKAction.group([scale, SoundLib.getSoundAction(SoundNames.Touch)])
            }
            
            if spriteBackButton.containsPoint(location) {
                spriteBackButton.runAction(action)
                currentZoomingButton = spriteBackButton
            }
            else {
                let insideLocation = touch.locationInNode(spriteGuideBackground)
                if spritePrevButton.containsPoint(insideLocation) {
                    spritePrevButton.runAction(action)
                    currentZoomingButton = spritePrevButton
                }
                else if spriteNextButton.containsPoint(insideLocation) {
                    spriteNextButton.runAction(action)
                    currentZoomingButton = spriteNextButton
                }
            }
        }
    }
    
    override func touchesEnded(touches: NSSet, withEvent event: UIEvent) {
        /* Called when a touch ends */
        
        for touch: AnyObject in touches {
            let action = SKAction.scaleTo(CGFloat(1), duration: 0.2)
            if (currentZoomingButton !== nil) {
                currentZoomingButton?.runAction(action)
            }
            
            if spriteBackButton === currentZoomingButton {
                let transition = SKTransition.doorsCloseHorizontalWithDuration(0.5)
                self.view!.presentScene(previousScene, transition: transition)
                self.removeAllActions()
                self.removeAllChildren()
            }
            else if spritePrevButton === currentZoomingButton {
                switch currentStep {
                case 1:
                    ++currentStep
                    spriteStep01.runAction(getActionForLeftSlideOut())
                    spriteStep02.position = CGPoint(x: size.width, y: spriteGuideBackground.size.height / 2 - 120)
                    spriteStep02.runAction(getActionForSlideIn())
                case 2:
                    ++currentStep
                    spriteStep02.runAction(getActionForLeftSlideOut())
                    spriteStep03.position = CGPoint(x: size.width, y: spriteGuideBackground.size.height / 2 - 120)
                    spriteStep03.runAction(getActionForSlideIn())
                case 3:
                    ++currentStep
                    spriteStep03.runAction(getActionForLeftSlideOut())
                    spriteStep04.position = CGPoint(x: size.width, y: spriteGuideBackground.size.height / 2 - 120)
                    spriteStep04.runAction(getActionForSlideIn())
                case 4:
                    ++currentStep
                    spriteStep04.runAction(getActionForLeftSlideOut())
                    spriteStep05.position = CGPoint(x: size.width, y: spriteGuideBackground.size.height / 2 - 120)
                    spriteStep05.runAction(getActionForSlideIn())
                case 5:
                    ++currentStep
                    spriteStep05.runAction(getActionForLeftSlideOut())
                    spriteStep06.position = CGPoint(x: size.width, y: spriteGuideBackground.size.height / 2 - 120)
                    spriteStep06.runAction(getActionForSlideIn())
                case 6:
                    // Move swing
                    currentStep = 1
                    spriteStep06.runAction(getActionForLeftSlideOut())
                    spriteStep01.position = CGPoint(x: size.width, y: spriteGuideBackground.size.height / 2 - 120)
                    spriteStep01.runAction(getActionForSlideIn())
                default:
                    return
                }
            }
            else if spriteNextButton === currentZoomingButton {
                switch currentStep {
                case 1:
                    // Moving swing
                    currentStep = 6
                    spriteStep01.runAction(getActionForRightSlideOut())
                    spriteStep06.position = CGPoint(x: -size.width, y: spriteGuideBackground.size.height / 2 - 120)
                    spriteStep06.runAction(getActionForSlideIn())
                case 2:
                    --currentStep
                    spriteStep02.runAction(getActionForRightSlideOut())
                    spriteStep01.position = CGPoint(x: -size.width, y: spriteGuideBackground.size.height / 2 - 120)
                    spriteStep01.runAction(getActionForSlideIn())
                case 3:
                    --currentStep
                    spriteStep03.runAction(getActionForRightSlideOut())
                    spriteStep02.position = CGPoint(x: -size.width, y: spriteGuideBackground.size.height / 2 - 120)
                    spriteStep02.runAction(getActionForSlideIn())
                case 4:
                    --currentStep
                    spriteStep04.runAction(getActionForRightSlideOut())
                    spriteStep03.position = CGPoint(x: -size.width, y: spriteGuideBackground.size.height / 2 - 120)
                    spriteStep03.runAction(getActionForSlideIn())
                case 5:
                    --currentStep
                    spriteStep05.runAction(getActionForRightSlideOut())
                    spriteStep04.position = CGPoint(x: -size.width, y: spriteGuideBackground.size.height / 2 - 120)
                    spriteStep04.runAction(getActionForSlideIn())
                case 6:
                    --currentStep
                    spriteStep06.runAction(getActionForRightSlideOut())
                    spriteStep05.position = CGPoint(x: -size.width, y: spriteGuideBackground.size.height / 2 - 120)
                    spriteStep05.runAction(getActionForSlideIn())
                default:
                    return
                }
            }
            
            currentZoomingButton = nil
        }
    }
    
    override func update(currentTime: CFTimeInterval) {
        /* Called before each frame is rendered */
    }
    
    func getActionForSlideIn() -> SKAction {
        let fadeIn = SKAction.fadeAlphaTo(1, duration: 0.2)
        let moveToX = SKAction.moveToX(0, duration: 0.2)
        
        return SKAction.group([fadeIn, moveToX])
    }
    
    func getActionForLeftSlideOut() -> SKAction {
        let fadeOut = SKAction.fadeAlphaTo(0, duration: 0.2)
        let moveToX = SKAction.moveToX(-size.width, duration: 0.2)
        
        return SKAction.group([fadeOut, moveToX])
    }
    
    func getActionForRightSlideOut() -> SKAction {
        let fadeOut = SKAction.fadeAlphaTo(0, duration: 0.2)
        let moveToX = SKAction.moveToX(size.width, duration: 0.2)
        
        return SKAction.group([fadeOut, moveToX])
    }
}
