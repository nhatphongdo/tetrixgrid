//
//  LeaderboardScene.swift
//  TetrixGrid
//
//  Created by Do Nhat Phong on 2/10/15.
//  Copyright (c) 2015 VietDev Corporation. All rights reserved.
//

import SpriteKit

class LeaderboardScene: SKScene {
    
    let AvatarSize: Int = 120
    
    var spriteBackground = SKSpriteNode(imageNamed: "Background")
    var spriteBackButton = SKSpriteNode(imageNamed: "Button_Quit")
    var spriteCopyrightText = SKSpriteNode(imageNamed: "Text_Copyright")
    var spriteTitle = SKSpriteNode(imageNamed: "Title")
    var spriteLeaderboardBackground = SKSpriteNode(imageNamed: "Background_Leaderboard")
    
    var previousScene: SKScene? = nil
    
    var currentZoomingButton: SKSpriteNode? = nil
    
    var loaded = false
    
    override func didMoveToView(view: SKView) {
        /* Setup your scene here */
        
        if loaded {
            return
        }
        
        let gap = CGFloat(20)
        var y = CGFloat(0)
        
        spriteBackground.position = CGPoint(x: size.width / 2, y: size.height / 2)
        self.addChild(spriteBackground)
        
        // Add title
        spriteTitle.position = CGPoint(x: size.width / 2, y: size.height - spriteTitle.size.height / 2 - gap)
        self.addChild(spriteTitle)
        
        // Add copyright text
        spriteCopyrightText.position = CGPoint(x: size.width / 2, y: gap + spriteCopyrightText.size.height / 2)
        self.addChild(spriteCopyrightText)
        
        // Add exit button
        spriteBackButton.position = CGPoint(x: gap + spriteBackButton.size.width / 2, y: gap + spriteBackButton.size.height / 2)
        self.addChild(spriteBackButton)
        
        // Add guide background
        spriteLeaderboardBackground.position = CGPoint(x: size.width / 2, y: size.height / 2)
        self.addChild(spriteLeaderboardBackground)
        
        loaded = true
    }
    
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        /* Called when a touch begins */
        
        for touch: AnyObject in touches {
            let location = touch.locationInNode(self)
            
            currentZoomingButton = nil
            let scale = SKAction.scaleTo(CGFloat(1.2), duration: 0.2)
            var action: SKAction = scale
            if UserData.UserSettings.soundEnabled {
                action = SKAction.group([scale, SoundLib.getSoundAction(SoundNames.Touch)])
            }
            
            if spriteBackButton.containsPoint(location) {
                spriteBackButton.runAction(action)
                currentZoomingButton = spriteBackButton
            }
        }
    }
    
    override func touchesEnded(touches: NSSet, withEvent event: UIEvent) {
        /* Called when a touch ends */
        
        for touch: AnyObject in touches {
            let action = SKAction.scaleTo(CGFloat(1), duration: 0.2)
            if (currentZoomingButton !== nil) {
                currentZoomingButton?.runAction(action)
            }

            if spriteBackButton === currentZoomingButton {
                let transition = SKTransition.doorsCloseHorizontalWithDuration(0.5)
                self.view!.presentScene(previousScene, transition: transition)
                self.removeAllActions()
                self.removeAllChildren()
            }
            
            currentZoomingButton = nil
        }
    }
    
    override func update(currentTime: CFTimeInterval) {
        /* Called before each frame is rendered */
    }
}
