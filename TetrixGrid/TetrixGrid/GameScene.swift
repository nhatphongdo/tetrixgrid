//
//  GameScene.swift
//  TetrixGrid
//
//  Created by Do Nhat Phong on 1/19/15.
//  Copyright (c) 2015 VietDev Corporation. All rights reserved.
//

import SpriteKit
import Social

class GameScene: SKScene {
    
    var menuScene: MenuScene? = nil
    
    var viewController: MenuViewController? = nil

    var spriteBackground = SKSpriteNode(imageNamed: "Background")
    var spriteBestScore = SKSpriteNode(imageNamed: "Background_Box_BestScore")
    var spriteScore = SKSpriteNode(imageNamed: "Background_Box_Score")
    var spriteTime = SKSpriteNode(imageNamed: "Background_Box_Time")
    var spriteProgressBar = SKSpriteNode(imageNamed: "Background_ProgressBar")
    var spriteProgressThumb = SKSpriteNode(imageNamed: "Background_ProgressThumb")
    var spritePauseButton = SKSpriteNode(imageNamed: "Button_Pause")
    var spritePausedPopup = SKSpriteNode(imageNamed: "Background_Paused_Popup")
    var spriteResumeButton = SKSpriteNode(imageNamed: "Button_Resume")
    var spriteRestartButton = SKSpriteNode(imageNamed: "Button_Restart")
    var spriteMenuButton = SKSpriteNode(imageNamed: "Button_Menu")
    var spriteSoundButton = SKSpriteNode(imageNamed: "Button_Paused_Sound")
    var spriteMusicButton = SKSpriteNode(imageNamed: "Button_Paused_Music")
    var spriteSoundOffOverlay = SKSpriteNode(imageNamed: "Overlay_Off")
    var spriteMusicOffOverlay = SKSpriteNode(imageNamed: "Overlay_Off")
    var spriteEndPopup = SKSpriteNode(imageNamed: "Background_Box_GameOver")
    var spriteEndRestartButton = SKSpriteNode(imageNamed: "Button_Restart")
    var spriteEndMenuButton = SKSpriteNode(imageNamed: "Button_Menu")
    var spriteEndShareButton = SKSpriteNode(imageNamed: "Button_ShareScore")
    var endScoreLabel = SKLabelNode(fontNamed: "ChalkboardSE-Bold")
    var bestScoreLabel = SKLabelNode(fontNamed: "ChalkboardSE-Bold")
    var scoreLabel = SKLabelNode(fontNamed: "ChalkboardSE-Bold")
    var timeLabel = SKLabelNode(fontNamed: "ChalkboardSE-Bold")
        
    // Bonus
    var spriteBombBonus = SKSpriteNode()
    var spriteBomb = SKSpriteNode(imageNamed: "Icon_Bomb")
    var bombCountLabel = SKLabelNode(fontNamed: "ChalkboardSE-Bold")
    
    var spriteRotateBonus = SKSpriteNode()
    var spriteRotate = SKSpriteNode(imageNamed: "Icon_Rotate")
    var rotateCountLabel = SKLabelNode(fontNamed: "ChalkboardSE-Bold")
    var spriteRotateTop = SKSpriteNode(imageNamed: "Icon_Rotate1")
    var spriteRotateBottom = SKSpriteNode(imageNamed: "Icon_Rotate2")
    var spriteRotateButtons = [SKSpriteNode]()
    var oldOrientations = [Orientation]()

    var spriteFillInBonus = SKSpriteNode()
    var spriteFillIn = SKSpriteNode(imageNamed: "Icon_FillIn")
    var fillInCountLabel = SKLabelNode(fontNamed: "ChalkboardSE-Bold")
    
    var pausePopup = SKSpriteNode()
    
    var endPopup = SKSpriteNode()
    
    var Settings: GameSettings = GameSettings()

    var tetrixGrid: TetrixGrid? = nil
    
    var currentZoomingButton: SKSpriteNode? = nil
    
    var currentMovingBlock: (Int, TetrixBlock?) = (-1, nil)
    
    var pausing = false
    var starting = false
    var ending = false
    var touching = false
    var animating = false
    var rotateMode = false
    
    var score: Int = 0
    
    var lastTime: Double = 0
    var currentTime: Double = 0
    
    var bombCount: Int = 0
    var rotateCount: Int = 0
    var fillInCount: Int = 0
    
    var consecutiveClear: Int = 0
    var consecutiveUnclear: Int = 0
    
    var loaded = false
    
    override func didMoveToView(view: SKView) {
        viewController?.hideBottomAd()
        
        /* Setup your scene here */
        if loaded {
            restart()
            return
        }
        
        if Settings.Mode == GameModes.Multiplayer {
            Settings.PlayableLeft = 0
            Settings.PlayableRight = Settings.TwoPlayerHorizontalBlockCount - 1
            Settings.PlayableTop = Settings.TwoPlayerVerticalBlockCount / 2
            Settings.PlayableBottom = Settings.TwoPlayerVerticalBlockCount - 1
        }
        else {
            Settings.PlayableLeft = 0
            Settings.PlayableRight = Settings.HorizontalBlockCount - 1
            Settings.PlayableTop = 0
            Settings.PlayableBottom = Settings.VerticalBlockCount - 1
        }

        tetrixGrid = TetrixGrid(settings: Settings)
        
        starting = true

        let gap = CGFloat(20)

        // Add background
        spriteBackground.position = CGPoint(x: size.width / 2, y: size.height / 2)
        self.addChild(spriteBackground)
        
        // Add best score box
        spriteBestScore.anchorPoint = CGPoint(x: 0, y: 1)
        spriteBestScore.position = CGPoint(x: gap, y: size.height - gap)
        self.addChild(spriteBestScore)
        
        bestScoreLabel.fontSize = 36
        bestScoreLabel.text = String(UserData.UserSettings.bestScore)
        bestScoreLabel.fontColor = UIColor(red: 221/255.0, green: 71/255.0, blue: 51/255.0, alpha: 1)
        bestScoreLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Center
        bestScoreLabel.verticalAlignmentMode = SKLabelVerticalAlignmentMode.Center
        bestScoreLabel.position = CGPoint(x: spriteBestScore.size.width / 2, y: -(spriteBestScore.size.height + 50) / 2)
        bestScoreLabel.zPosition = 90
        spriteBestScore.addChild(bestScoreLabel)
        
        // Add score box
        spriteScore.anchorPoint = CGPoint(x: 0.5, y: 1)
        spriteScore.position = CGPoint(x: size.width / 2, y: size.height - gap)
        self.addChild(spriteScore)
        
        scoreLabel.fontSize = 36
        scoreLabel.text = "0"
        scoreLabel.fontColor = UIColor(red: 79/255.0, green: 150/255.0, blue: 45/255.0, alpha: 1)
        scoreLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Center
        scoreLabel.verticalAlignmentMode = SKLabelVerticalAlignmentMode.Center
        scoreLabel.position = CGPoint(x: 0, y: -(spriteScore.size.height + 50) / 2)
        scoreLabel.zPosition = 90
        spriteScore.addChild(scoreLabel)
        
        // Add time box
        spriteTime.anchorPoint = CGPoint(x: 1, y: 1)
        spriteTime.position = CGPoint(x: size.width - gap, y: size.height - gap)
        self.addChild(spriteTime)
        
        timeLabel.fontSize = 36
        timeLabel.text = "00:00"
        timeLabel.fontColor = UIColor(red: 39/255.0, green: 39/255.0, blue: 39/255.0, alpha: 1)
        timeLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Center
        timeLabel.verticalAlignmentMode = SKLabelVerticalAlignmentMode.Center
        timeLabel.position = CGPoint(x: -spriteTime.size.width / 2, y: -(spriteTime.size.height + 50) / 2)
        timeLabel.zPosition = 90
        spriteTime.addChild(timeLabel)
        
        // Add pause button
        spritePauseButton.position = CGPoint(x: gap + spritePauseButton.size.width / 2, y: size.height - spriteTime.size.height - gap - spritePauseButton.size.height / 2 - gap)
        self.addChild(spritePauseButton)
        
        // Add bomb bonus
        spriteBombBonus.anchorPoint = CGPoint(x: 0, y: 0.5)
        spriteBombBonus.position = CGPoint(x: 170, y: size.height - spriteTime.size.height - gap - spritePauseButton.size.height / 2 - gap)
        self.addChild(spriteBombBonus)
        spriteBomb.anchorPoint = CGPoint(x: 0, y: 0.5)
        spriteBomb.position = CGPoint(x: 0, y: 0)
        spriteBombBonus.addChild(spriteBomb)
        bombCountLabel.fontSize = 40
        bombCountLabel.text = "x" + String(bombCount)
        bombCountLabel.fontColor = UIColor(red: 221/255.0, green: 71/255.0, blue: 51/255.0, alpha: 1)
        bombCountLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Left
        bombCountLabel.verticalAlignmentMode = SKLabelVerticalAlignmentMode.Center
        bombCountLabel.position = CGPoint(x: spriteBomb.size.width, y: -spriteBombBonus.size.height / 2 - 10)
        bombCountLabel.zPosition = 90
        spriteBombBonus.addChild(bombCountLabel)
        spriteBomb.color = UIColor.grayColor()
        bombCountLabel.color = UIColor.grayColor()
        if bombCount == 0 {
            spriteBomb.colorBlendFactor = 1.0
            bombCountLabel.colorBlendFactor = 1.0
        }

        // Add rotate bonus
        spriteRotateBonus.anchorPoint = CGPoint(x: 0, y: 0.5)
        spriteRotateBonus.position = CGPoint(x: 323, y: size.height - spriteTime.size.height - gap - spritePauseButton.size.height / 2 - gap)
        self.addChild(spriteRotateBonus)
        spriteRotate.anchorPoint = CGPoint(x: 0, y: 0.5)
        spriteRotate.position = CGPoint(x: 0, y: -10)
        spriteRotateBonus.addChild(spriteRotate)
        rotateCountLabel.fontSize = 40
        rotateCountLabel.text = "x" + String(rotateCount)
        rotateCountLabel.fontColor = UIColor(red: 221/255.0, green: 71/255.0, blue: 51/255.0, alpha: 1)
        rotateCountLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Left
        rotateCountLabel.verticalAlignmentMode = SKLabelVerticalAlignmentMode.Center
        rotateCountLabel.position = CGPoint(x: spriteRotate.size.width + 10, y: -spriteRotateBonus.size.height / 2 - 10)
        rotateCountLabel.zPosition = 90
        spriteRotateBonus.addChild(rotateCountLabel)
        spriteRotate.color = UIColor.grayColor()
        rotateCountLabel.color = UIColor.grayColor()
        if rotateCount == 0 {
            spriteRotate.colorBlendFactor = 1.0
            rotateCountLabel.colorBlendFactor = 1.0
        }
//        spriteRotateTop.hidden = true
//        spriteRotateBottom.hidden = true
//        spriteRotateButton.hidden = true
//        self.addChild(spriteRotateTop)
//        self.addChild(spriteRotateBottom)
//        self.addChild(spriteRotateButton)
        
        // Add fill in bonus
        spriteFillInBonus.anchorPoint = CGPoint(x: 0, y: 0.5)
        spriteFillInBonus.position = CGPoint(x: 483, y: size.height - spriteTime.size.height - gap - spritePauseButton.size.height / 2 - gap)
        self.addChild(spriteFillInBonus)
        spriteFillIn.anchorPoint = CGPoint(x: 0, y: 0.5)
        spriteFillIn.position = CGPoint(x: 0, y: -10)
        spriteFillInBonus.addChild(spriteFillIn)
        fillInCountLabel.fontSize = 40
        fillInCountLabel.text = "x" + String(fillInCount)
        fillInCountLabel.fontColor = UIColor(red: 221/255.0, green: 71/255.0, blue: 51/255.0, alpha: 1)
        fillInCountLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Left
        fillInCountLabel.verticalAlignmentMode = SKLabelVerticalAlignmentMode.Center
        fillInCountLabel.position = CGPoint(x: spriteFillIn.size.width + 10, y: -spriteFillInBonus.size.height / 2 - 10)
        fillInCountLabel.zPosition = 90
        spriteFillInBonus.addChild(fillInCountLabel)
        spriteFillIn.color = UIColor.grayColor()
        fillInCountLabel.color = UIColor.grayColor()
        if fillInCount == 0 {
            spriteFillIn.colorBlendFactor = 1.0
            fillInCountLabel.colorBlendFactor = 1.0
        }

        tetrixGrid!.GridNode.position = CGPoint(x: (size.width - tetrixGrid!.GridNode.size.width) / 2, y: (size.height + tetrixGrid!.GridNode.size.height) / 2)
        self.addChild(tetrixGrid!.GridNode)
        
        // Add next blocks
        let nextScales = getNextBlocksScale()
        tetrixGrid!.NextBlockNode.xScale = nextScales.0
        tetrixGrid!.NextBlockNode.yScale = nextScales.0
        tetrixGrid!.NextBlockNode.position = CGPoint(x: (size.width - tetrixGrid!.NextBlockNode.size.width) / 2, y: 50 + tetrixGrid!.NextBlockNode.size.height)
        self.addChild(tetrixGrid!.NextBlockNode)
        
        // Pause popup
        pausePopup.alpha = 0
        pausePopup.size = size
        pausePopup.position = CGPoint(x: size.width / 2, y: size.height / 2)
        pausePopup.setScale(0)
        pausePopup.zPosition = 999
        self.addChild(pausePopup)
        
        let pauseBackground = SKSpriteNode(color: UIColor.blackColor(), size: size)
        pauseBackground.alpha = 0.7
        pauseBackground.position = CGPoint(x: 0, y: 0)
        pausePopup.addChild(pauseBackground)
        
        // Pause background
        spritePausedPopup.position = CGPoint(x: 0, y: 0)
        pausePopup.addChild(spritePausedPopup)
        
        // Resume button
        spriteResumeButton.position = CGPoint(x: -150, y: -25)
        spritePausedPopup.addChild(spriteResumeButton)
        
        // Replay button
        spriteRestartButton.position = CGPoint(x: 0, y: -25)
        spritePausedPopup.addChild(spriteRestartButton)
        
        // Menu button
        spriteMenuButton.position = CGPoint(x: 150, y: -25)
        spritePausedPopup.addChild(spriteMenuButton)

        // Sound button
        spriteSoundButton.position = CGPoint(x: -spriteSoundButton.size.width / 2 - 2 * gap, y: -size.height / 2 + spriteSoundButton.size.height / 2 + gap)
        pausePopup.addChild(spriteSoundButton)
        // Off overlay
        spriteSoundOffOverlay.zPosition = 99
        spriteSoundOffOverlay.position = CGPoint(x: -spriteSoundButton.size.width / 2 - 2 * gap, y: -size.height / 2 + spriteSoundButton.size.height / 2 + gap + 5)
        pausePopup.addChild(spriteSoundOffOverlay)

        // Music button
        spriteMusicButton.position = CGPoint(x: spriteMusicButton.size.width / 2 + 2 * gap, y:  -size.height / 2 + spriteSoundButton.size.height / 2 + gap)
        pausePopup.addChild(spriteMusicButton)
        // Off overlay
        spriteMusicOffOverlay.zPosition = 99
        spriteMusicOffOverlay.position = CGPoint(x: spriteMusicButton.size.width / 2 + 2 * gap, y:  -size.height / 2 + spriteSoundButton.size.height / 2 + gap + 5)
        pausePopup.addChild(spriteMusicOffOverlay)

        if UserData.UserSettings.soundEnabled {
            spriteSoundOffOverlay.alpha = 0
        }
        else {
            spriteSoundOffOverlay.alpha = 1
        }
        if UserData.UserSettings.musicEnabled {
            spriteMusicOffOverlay.alpha = 0
        }
        else {
            spriteMusicOffOverlay.alpha = 1
        }
        
        // End popup
        endPopup.alpha = 0
        endPopup.size = size
        endPopup.position = CGPoint(x: size.width / 2, y: size.height / 2)
        endPopup.setScale(0)
        endPopup.zPosition = 999
        self.addChild(endPopup)
        
        let endBackground = SKSpriteNode(color: UIColor.blackColor(), size: size)
        endBackground.alpha = 0.7
        endBackground.position = CGPoint(x: 0, y: 0)
        endPopup.addChild(endBackground)
        
        // Game over background
        spriteEndPopup.position = CGPoint(x: 0, y: 0)
        endPopup.addChild(spriteEndPopup)
        
        // Score label
        endScoreLabel.fontSize = 60
        endScoreLabel.text = "0"
        endScoreLabel.fontColor = UIColor(red: 79/255.0, green: 150/255.0, blue: 45/255.0, alpha: 1)
        endScoreLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Left
        endScoreLabel.verticalAlignmentMode = SKLabelVerticalAlignmentMode.Center
        endScoreLabel.position = CGPoint(x: 70, y: 80)
        endScoreLabel.zPosition = 90
        spriteEndPopup.addChild(endScoreLabel)
        
        // Replay button
        spriteEndRestartButton.position = CGPoint(x: -100, y: -10)
        spriteEndRestartButton.zPosition = 90
        spriteEndPopup.addChild(spriteEndRestartButton)
        
        // Menu button
        spriteEndMenuButton.position = CGPoint(x: 100, y: -10)
        spriteEndMenuButton.zPosition = 90
        spriteEndPopup.addChild(spriteEndMenuButton)
        
        // Share score button
        spriteEndShareButton.position = CGPoint(x: 0, y: -120)
        spriteEndShareButton.zPosition = 90
        spriteEndPopup.addChild(spriteEndShareButton)

        if UserData.UserSettings.musicEnabled {
            SoundLib.playBackground(spriteBackground)
        }
        
        loaded = true
    }
    
    func restart() {
        // Clear popup
        let scale = SKAction.scaleTo(0, duration: 0.2)
        let fadeOut = SKAction.fadeAlphaTo(0, duration: 0.2)
        let group = SKAction.group([scale, fadeOut])
        pausePopup.runAction(group)
        endPopup.runAction(group)
        
        // Clear state
        ending = false
        pausing = false
        starting = true
        touching = false
        animating = false
        rotateMode = false

        currentZoomingButton = nil
        currentMovingBlock = (-1, nil)
        
        lastTime = 0
        currentTime = 0
        score = 0
        bombCount = 0
        rotateCount = 0
        fillInCount = 0
        consecutiveClear = 0
        consecutiveUnclear = 0
        tetrixGrid!.reset()
        
        let nextScales = getNextBlocksScale()
        tetrixGrid!.NextBlockNode.position = CGPoint(x: (size.width - nextScales.0 * tetrixGrid!.NextBlockNode.size.width) / 2, y: (50 + nextScales.1 * tetrixGrid!.NextBlockNode.size.height))
        
        for button in spriteRotateButtons {
            button.removeFromParent()
        }
        spriteRotateButtons.removeAll(keepCapacity: false)

        // Clear interface
        scoreLabel.text = "0"
        timeLabel.text = "00:00"
        bombCountLabel.text = "x0"
        rotateCountLabel.text = "x0"
        fillInCountLabel.text = "x0"
        spriteBomb.colorBlendFactor = 1.0
        spriteBomb.color = UIColor.grayColor()
        bombCountLabel.colorBlendFactor = 1.0
        bombCountLabel.color = UIColor.grayColor()
        spriteRotate.colorBlendFactor = 1.0
        spriteRotate.color = UIColor.grayColor()
        rotateCountLabel.colorBlendFactor = 1.0
        rotateCountLabel.color = UIColor.grayColor()
        spriteFillIn.colorBlendFactor = 1.0
        spriteFillIn.color = UIColor.grayColor()
        fillInCountLabel.colorBlendFactor = 1.0
        fillInCountLabel.color = UIColor.grayColor()
        
        spriteBackground.removeAllActions()
        if UserData.UserSettings.musicEnabled {
            SoundLib.playBackground(spriteBackground)
        }
    }
    
    func getNextBlocksScale() -> (CGFloat, CGFloat) {
        let maxNextBlockWidth = CGFloat(Settings.NextBlockCount * 250 + (Settings.NextBlockCount - 1) * 40) // 250 is max width of each block
        let maxNextBlockHeight = CGFloat(250) // 250 is max height of each block
        let xScale = CGFloat(640.0) / maxNextBlockWidth
        
        return (xScale, xScale)
    }
    
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        /* Called when a touch begins */
        if touching || animating {
            return
        }
        
        touching = true
        
        for touch: AnyObject in touches {
            let location = touch.locationInNode(self)
         
            currentMovingBlock = (-1, nil)
            currentZoomingButton = nil
            let scale = SKAction.scaleTo(CGFloat(1.2), duration: 0.2)
            var action: SKAction = scale
            if UserData.UserSettings.soundEnabled {
                action = SKAction.group([scale, SoundLib.getSoundAction(SoundNames.Touch)])
            }
            
            if spritePauseButton.containsPoint(location) {
                spritePauseButton.runAction(action)
                currentZoomingButton = spritePauseButton
            }
            else if rotateMode == false && bombCount > 0 && spriteBombBonus.containsPoint(location) {
                spriteBombBonus.runAction(action)
                currentZoomingButton = spriteBombBonus

                currentMovingBlock = (Settings.NextBlockCount + 1, TetrixBlock(type: BlockType.Dot, block: BlockCode.Bomb, orientation: Orientation.Zero, size: Settings.GridBlockSize))
                currentMovingBlock.1!.BlockNode.position = spriteBombBonus.position
                currentMovingBlock.1!.BlockNode.zPosition = 100
                self.addChild(currentMovingBlock.1!.BlockNode)
                if currentMovingBlock.0 != -1 && currentMovingBlock.1 != nil {
                    let locationInNode = touch.locationInNode(currentMovingBlock.1!.BlockNode)
                    let shiftY = max(0, currentMovingBlock.1!.BlockNode.size.height + locationInNode.y)
                    currentMovingBlock.1!.BlockNode.runAction(getActionForPickBlock(-0.05 * currentMovingBlock.1!.BlockNode.size.width, deltaY: 100 + shiftY, scale: 1, closure: { () -> () in
                    }))
                }
            }
            else if rotateMode == false && rotateCount > 0 && spriteRotateBonus.containsPoint(location) {
                spriteRotateBonus.runAction(action)
                currentZoomingButton = spriteRotateBonus
            }
            else if rotateCount > 0 && rotateMode {
                for button in spriteRotateButtons {
                    if button.containsPoint(location) {
                        button.runAction(action)
                        currentZoomingButton = button
                    }
                }
            }
            else if rotateMode == false && fillInCount > 0 && spriteFillInBonus.containsPoint(location) {
                spriteFillInBonus.runAction(action)
                currentZoomingButton = spriteFillInBonus

                currentMovingBlock = (Settings.NextBlockCount, TetrixBlock(type: BlockType.Dot, block: BlockCode.FillIn, orientation: Orientation.Zero, size: Settings.GridBlockSize))
                currentMovingBlock.1!.BlockNode.position = spriteFillInBonus.position
                currentMovingBlock.1!.BlockNode.zPosition = 100
                self.addChild(currentMovingBlock.1!.BlockNode)
                if currentMovingBlock.0 != -1 && currentMovingBlock.1 != nil {
                    let locationInNode = touch.locationInNode(currentMovingBlock.1!.BlockNode)
                    let shiftY = max(0, currentMovingBlock.1!.BlockNode.size.height + locationInNode.y)
                    currentMovingBlock.1!.BlockNode.runAction(getActionForPickBlock(-0.05 * currentMovingBlock.1!.BlockNode.size.width, deltaY: 100 + shiftY, scale: 1, closure: { () -> () in
                    }))
                }
            }
            else {
                if pausing == true {
                    let pauseLocation = touch.locationInNode(spritePausedPopup)
                    if spriteResumeButton.containsPoint(pauseLocation) {
                        spriteResumeButton.runAction(action)
                        currentZoomingButton = spriteResumeButton
                    }
                    else if spriteRestartButton.containsPoint(pauseLocation) {
                        spriteRestartButton.runAction(action)
                        currentZoomingButton = spriteRestartButton
                    }
                    else if spriteMenuButton.containsPoint(pauseLocation) {
                        spriteMenuButton.runAction(action)
                        currentZoomingButton = spriteMenuButton
                    }
                    else {
                        let settingsLocation = touch.locationInNode(pausePopup)
                        if spriteSoundButton.containsPoint(settingsLocation) {
                            spriteSoundButton.runAction(action)
                            currentZoomingButton = spriteSoundButton
                        }
                        else if spriteMusicButton.containsPoint(settingsLocation) {
                            spriteMusicButton.runAction(action)
                            currentZoomingButton = spriteMusicButton
                        }
                    }
                }
                else if ending == true {
                    let endLocation = touch.locationInNode(spriteEndPopup)
                    if spriteEndRestartButton.containsPoint(endLocation) {
                        spriteEndRestartButton.runAction(action)
                        currentZoomingButton = spriteEndRestartButton
                    }
                    else if spriteEndMenuButton.containsPoint(endLocation) {
                        spriteEndMenuButton.runAction(action)
                        currentZoomingButton = spriteEndMenuButton
                    }
                    else if spriteEndShareButton.containsPoint(endLocation) {
                        spriteEndShareButton.runAction(action)
                        currentZoomingButton = spriteEndShareButton
                    }
                }
                else {
                    if rotateMode == false {
                        currentMovingBlock = tetrixGrid!.findNextBlockInTouch(touch)
                        if currentMovingBlock.0 != -1 && currentMovingBlock.1 != nil {
                            let locationInNode = touch.locationInNode(currentMovingBlock.1!.BlockNode)
                            let shiftY = max(0, currentMovingBlock.1!.BlockNode.size.height + locationInNode.y)
                            currentMovingBlock.1!.BlockNode.runAction(getActionForPickBlock(-0.05 * currentMovingBlock.1!.BlockNode.size.width, deltaY: 100 + shiftY, scale: 1.1, closure: { () -> () in
                            }))
                        }
                    }
                }
            }
        }
    }
    
    override func touchesEnded(touches: NSSet, withEvent event: UIEvent) {
        /* Called when a touch ends */
        if touching == false {
            return
        }
        
        touching = false
        
        for touch: AnyObject in touches {
            let location = touch.locationInNode(self)
            
            let action = SKAction.scaleTo(CGFloat(1), duration: 0.2)
            if (currentZoomingButton != nil) {
                currentZoomingButton!.runAction(action)
            }
            
            if pausing == false && spritePauseButton === currentZoomingButton {
                // Show pause menu
                pausing = true
                pausePopup.runAction(getActionForShowPopup())
                SoundLib.stopBackground(spriteBackground)
            }
            else if pausing == false && rotateMode == false && rotateCount > 0 && spriteRotateBonus == currentZoomingButton {
                // Change to rotate mode
                animateBonusOut(self.spriteRotateBonus, closure: { () -> () in
                })
                
                tetrixGrid!.NextBlockNode.runAction(SKAction.moveBy(CGVector(dx: 0, dy: 20), duration: 0.5))
                
                for button in spriteRotateButtons {
                    button.removeFromParent()
                }
                spriteRotateButtons.removeAll(keepCapacity: false)
                let nextScales = getNextBlocksScale()
                for child in tetrixGrid!.NextBlockNode.children as [SKSpriteNode] {
                    // Add rotate confirm button
                    var button = SKSpriteNode(imageNamed: "Button_Rotate")
                    button.position = CGPoint(x: child.position.x * nextScales.0 + tetrixGrid!.NextBlockNode.position.x + child.size.width * nextScales.0 / 2, y: button.size.height / 2 + 5)
                    self.addChild(button)
                    spriteRotateButtons.append(button)
                }
                
                oldOrientations.removeAll(keepCapacity: false)
                for block in tetrixGrid!.nextBlocks {
                    oldOrientations.append(block.orientation)
                }
                
                rotateMode = true
            }
            else if pausing == true && spriteResumeButton === currentZoomingButton {
                pausing = false
                pausePopup.runAction(getActionForHidePopup())
                if UserData.UserSettings.musicEnabled {
                    SoundLib.playBackground(spriteBackground)
                }
            }
            else if (pausing == true && spriteRestartButton === currentZoomingButton) || (ending == true && spriteEndRestartButton === currentZoomingButton) {
                SoundLib.stopBackground(spriteBackground)
                restart()
                break
            }
            else if (pausing == true && spriteMenuButton === currentZoomingButton) || (ending == true && spriteEndMenuButton === currentZoomingButton) {
                SoundLib.stopBackground(spriteBackground)
                restart()
                let transition = SKTransition.doorsCloseHorizontalWithDuration(0.5)
                self.view!.presentScene(menuScene, transition: transition)
            }
            else if pausing == true && spriteSoundButton == currentZoomingButton {
                if UserData.UserSettings.soundEnabled {
                    UserData.UserSettings.soundEnabled = false
                    spriteSoundOffOverlay.alpha = 1
                }
                else {
                    UserData.UserSettings.soundEnabled = true
                    spriteSoundOffOverlay.alpha = 0
                }
                UserData.setUserData("SoundEnabled", value: UserData.UserSettings.soundEnabled)
            }
            else if pausing == true && spriteMusicButton == currentZoomingButton {
                if UserData.UserSettings.musicEnabled {
                    UserData.UserSettings.musicEnabled = false
                    spriteMusicOffOverlay.alpha = 1
                }
                else {
                    UserData.UserSettings.musicEnabled = true
                    spriteMusicOffOverlay.alpha = 0
                }
                UserData.setUserData("MusicEnabled", value: UserData.UserSettings.musicEnabled)   
            }
            else if ending == true && spriteEndShareButton === currentZoomingButton {
                // Share current score to FB
                // Generate score image
                shareToFacebook("I scored " + String(score) + " in TeBreak. Do you want to challenge me?", image: generateScoreImage(score))
            }
            else if rotateMode == true && currentZoomingButton != nil {
                for (index, button) in enumerate(spriteRotateButtons) {
                    button.removeFromParent()

                    if button == currentZoomingButton {
                        // Keep this rotation, ignore the others
                        --rotateCount
                        animateBonusOut(self.spriteRotateBonus, closure: { () -> () in
                            self.rotateCountLabel.text = "x" + String(self.rotateCount)
                            if self.rotateCount == 0 {
                                self.spriteRotate.colorBlendFactor = 1
                                self.rotateCountLabel.colorBlendFactor = 1
                            }
                        })
                        
                        for (idx, ori) in enumerate(oldOrientations) {
                            if idx != index {
                                tetrixGrid!.nextBlocks[idx].orientation = ori
                                tetrixGrid!.nextBlocks[idx].recreate()
                            }
                        }
                        
                        let nextScales = getNextBlocksScale()
                        tetrixGrid!.redrawNextBlocks(false)
                        tetrixGrid!.NextBlockNode.runAction(SKAction.moveTo(CGPoint(x: (size.width - nextScales.0 * tetrixGrid!.NextBlockNode.size.width) / 2, y: (50 + nextScales.1 * tetrixGrid!.NextBlockNode.size.height)), duration: 0.5))
                        
                        let lose = self.tetrixGrid!.isLost()
                        if tetrixGrid!.isFull() || (bombCount == 0 && fillInCount == 0 && rotateCount == 0 && lose) {
                            gameOver()
                            break
                        }
                    }
                }
                
                spriteRotateButtons.removeAll(keepCapacity: false)
                rotateMode = false
            }
            else {
                if currentMovingBlock.0 == Settings.NextBlockCount && currentMovingBlock.1 != nil {
                    // Fill-in drop
                    tetrixGrid!.clearAllHighligh()

                    var canRelease = true
                    let coveredBlocks = tetrixGrid!.getCoverBlocks(currentMovingBlock.1!, touch: touch)
                    for item in coveredBlocks {
                        if item.0 < Settings.PlayableLeft || item.0 > Settings.PlayableRight || item.1 < Settings.PlayableTop || item.1 > Settings.PlayableBottom || tetrixGrid!.grid[item.1][item.0] != BlockCode.Empty {
                            canRelease = false
                            break
                        }
                    }
                    
                    if canRelease {
                        --fillInCount
                        animateBonusOut(self.spriteFillInBonus, closure: { () -> () in
                            self.fillInCountLabel.text = "x" + String(self.fillInCount)
                            if self.fillInCount == 0 {
                                self.spriteFillIn.colorBlendFactor = 1
                                self.fillInCountLabel.colorBlendFactor = 1
                            }
                        })

                        for item in coveredBlocks {
                            tetrixGrid!.grid[item.1][item.0] = BlockCode.random()
                        }
                        
                        tetrixGrid!.redraw()
                        
                        // Check clear state
                        if self.clearBlocks() == false {
                            return
                        }
                    }
                    
                    if currentMovingBlock.0 == Settings.NextBlockCount && currentMovingBlock.1 != nil {
                        self.currentMovingBlock.1!.BlockNode.runAction(getActionForDropBlock(spriteFillInBonus.position.x, y: spriteFillInBonus.position.y, sX: 1, sY: 1, failed: true, remove: true, closure: { () -> () in
                            self.tetrixGrid!.clearAllHighligh()
//                            if self.currentMovingBlock.1 != nil {
//                                self.currentMovingBlock.1!.BlockNode.removeFromParent()
//                            }
                            self.currentMovingBlock = (-1, nil)
                        }))
                    }
                }
                else if currentMovingBlock.0 == Settings.NextBlockCount + 1 && currentMovingBlock.1 != nil {
                    // Bomb drop
                    tetrixGrid!.clearAllHighligh()
                    
                    var canRelease = true
                    let coveredBlocks = tetrixGrid!.getCoverBlocks(currentMovingBlock.1!, touch: touch)
                    for item in coveredBlocks {
                        if item.0 < Settings.PlayableLeft || item.0 > Settings.PlayableRight || item.1 < Settings.PlayableTop || item.1 > Settings.PlayableBottom {
                            canRelease = false
                            break
                        }
                    }
                    
                    if canRelease {
                        for item in coveredBlocks {
                            // Explode a 3-cell square
                            for row in (item.1 - 1)...(item.1 + 1) {
                                for col in (item.0 - 1)...(item.0 + 1) {
                                    if col >= Settings.PlayableLeft && col <= Settings.PlayableRight && row >= Settings.PlayableTop && row <= Settings.PlayableBottom {
                                        tetrixGrid!.grid[row][col] = BlockCode.Empty
                                    }
                                }
                            }
                        }
                        
                        tetrixGrid!.redraw()
                        
                        --bombCount
                        animateBonusOut(self.spriteBombBonus, closure: { () -> () in
                            self.bombCountLabel.text = "x" + String(self.bombCount)
                            if self.bombCount == 0 {
                                self.spriteBomb.colorBlendFactor = 1
                                self.bombCountLabel.colorBlendFactor = 1
                            }
                        })
                        
                        let lose = self.tetrixGrid!.isLost()
                        if tetrixGrid!.isFull() || (bombCount == 0 && fillInCount == 0 && rotateCount == 0 && lose) {
                            gameOver()
                        }
                    }

                    if currentMovingBlock.0 == Settings.NextBlockCount + 1 && currentMovingBlock.1 != nil {
                        self.currentMovingBlock.1!.BlockNode.runAction(getActionForDropBlock(spriteBombBonus.position.x, y: spriteBombBonus.position.y, sX: 1, sY: 1, failed: true, remove: true, closure: { () -> () in
                            self.tetrixGrid!.clearAllHighligh()
//                            if self.currentMovingBlock.1 != nil {
//                                self.currentMovingBlock.1!.BlockNode.removeFromParent()
//                            }
                            self.currentMovingBlock = (-1, nil)
                        }))
                    }
                }
                else if currentMovingBlock.0 != -1 && currentMovingBlock.1 != nil {
                    // Release block
                    let canRelease = tetrixGrid!.releaseBlock(currentMovingBlock.1!, touch: touch)

                    if canRelease.0 {
                        self.score += self.currentMovingBlock.1!.count()

                        let nextScales = getNextBlocksScale()
                        var positionInGrid = tetrixGrid!.NextBlockNode.convertPoint(CGPoint(x: canRelease.1, y: canRelease.2), fromNode: self)
                        animating = true
                        self.currentMovingBlock.1!.BlockNode.runAction(getActionForDropBlock(positionInGrid.x, y: positionInGrid.y, sX: 1 / nextScales.0, sY: 1 / nextScales.1, failed: false, remove: false, closure: { () -> () in
                            self.tetrixGrid!.redraw()
                            
                            // Remove this block in next queue
                            if self.currentMovingBlock.0 != -1 && self.currentMovingBlock.1 != nil {
                                self.tetrixGrid!.removeNextBlock(self.currentMovingBlock.0)
                                self.currentMovingBlock.1!.BlockNode.removeFromParent()
                                self.currentMovingBlock = (-1, nil)
                            }

                            // Check clear state
                            if self.clearBlocks() == false {
                                return
                            }
                            
                            // Generate next blocks
                            if self.tetrixGrid!.isEmptyNextQueue() {
                                self.tetrixGrid!.generateNextBlocks()
                                let nextScales = self.getNextBlocksScale()
                                self.tetrixGrid!.NextBlockNode.position = CGPoint(x: (self.size.width - nextScales.0 * self.tetrixGrid!.NextBlockNode.size.width) / 2, y: (50 + nextScales.1 * self.tetrixGrid!.NextBlockNode.size.height))
                            }
                            
                            self.animating = false
                        }))
                    }
                    else {
                        if currentMovingBlock.0 != -1 && currentMovingBlock.1 != nil {
                            let block = tetrixGrid!.getNextBlock(currentMovingBlock.0)
                            self.currentMovingBlock.1!.BlockNode.runAction(getActionForDropBlock(block.x, y: block.y, sX: 1, sY: 1, failed: true, remove: false, closure: { () -> () in
                                self.tetrixGrid!.clearAllHighligh()
                            }))
                        }
                    }
                }
                else {
                    // Touch outside or touch in next block within rotate mode
                    if rotateMode {
                        let nextScales = getNextBlocksScale()
                        let nextTouchedBlock = tetrixGrid!.findNextBlockInTouch(touch)
                        if nextTouchedBlock.0 != -1 && nextTouchedBlock.1 != nil {
                            // Rotate this block in clockwise
                            tetrixGrid!.nextBlocks[nextTouchedBlock.0].orientation = Orientation.rotate(tetrixGrid!.nextBlocks[nextTouchedBlock.0].orientation, clockwise: true)
                            tetrixGrid!.nextBlocks[nextTouchedBlock.0].recreate()
                            tetrixGrid!.redrawNextBlocks(false)
                            for (idx, child) in enumerate(tetrixGrid!.NextBlockNode.children as [SKSpriteNode]) {
                                // Add rotate confirm button
                                spriteRotateButtons[idx].position = CGPoint(x: child.position.x * nextScales.0 + tetrixGrid!.NextBlockNode.position.x + child.size.width * nextScales.0 / 2, y: spriteRotateButtons[idx].size.height / 2 + 5)
                            }

                            tetrixGrid!.NextBlockNode.runAction(SKAction.moveTo(CGPoint(x: (size.width - nextScales.0 * tetrixGrid!.NextBlockNode.size.width) / 2, y: (70 + nextScales.1 * tetrixGrid!.NextBlockNode.size.height)), duration: 0.5))
                        }
                        else {
                            // Touch outside, exit rotate mode
                            for button in spriteRotateButtons {
                                button.removeFromParent()
                            }
                            spriteRotateButtons.removeAll(keepCapacity: false)
                            
                            // Restore
                            for (idx, ori) in enumerate(oldOrientations) {
                                tetrixGrid!.nextBlocks[idx].orientation = ori
                                tetrixGrid!.nextBlocks[idx].recreate()
                            }

                            tetrixGrid!.redrawNextBlocks(false)
                            tetrixGrid!.NextBlockNode.runAction(SKAction.moveTo(CGPoint(x: (size.width - nextScales.0 * tetrixGrid!.NextBlockNode.size.width) / 2, y: (50 + nextScales.1 * tetrixGrid!.NextBlockNode.size.height)), duration: 0.5))
                            rotateMode = false
                        }
                    }
                }
            }
        }
    }
    
    override func touchesMoved(touches: NSSet, withEvent event: UIEvent) {
        /* Called when a touch moves */
        
        if rotateMode {
            return
        }
        
        for touch: AnyObject in touches {
            let location = touch.locationInNode(self)
            let prevLocation = touch.previousLocationInNode(self)
            let nextScales = getNextBlocksScale()
            var translation = CGVector(dx: (1 / nextScales.0) * (location.x - prevLocation.x), dy: (1 / nextScales.1) * (location.y - prevLocation.y))
            
            if currentMovingBlock.0 != -1 && currentMovingBlock.1 != nil {
               
                if currentMovingBlock.0 >= Settings.NextBlockCount {
                    translation = CGVector(dx: (location.x - prevLocation.x), dy: (location.y - prevLocation.y))
                }
                
                // Move block
                let action = SKAction.moveBy(translation, duration: 0.0)
                currentMovingBlock.1!.BlockNode.runAction(action)
                
                // Highligh grid, using delay to avoid decreasing FPS
                delayed(0.3, name: "HighlightGridBlock", closure: {() -> () in
                    // Only highlight if stop on block within 0.5 second
                    if self.currentMovingBlock.0 != -1 && self.currentMovingBlock.1 != nil {
                        self.tetrixGrid!.highlightBlocksInGrid(self.currentMovingBlock.1!, touch: touch, bombOverlay: (self.currentMovingBlock.0 == self.Settings.NextBlockCount + 1))
                    }
                })
                
                if abs(translation.dx) > 5 || abs(translation.dy) > 5 {
                    // Change position then not highlight
                    cancelDelayed("HighlightGridBlock")
                }
            }
        }
    }
   
    override func update(currentTime: CFTimeInterval) {
        /* Called before each frame is rendered */
        if starting {
            starting = false
            self.lastTime = currentTime
        }
        
        let interval = currentTime - self.lastTime
        if pausing == false && ending == false {
            // Game is running
            self.currentTime += interval
            let min = NSString(format: "%02d", Int((self.currentTime) / 60))
            let sec = NSString(format: "%02d", Int((self.currentTime) % 60))
            timeLabel.text = (String(min) + ":" + String(sec))
        }
        self.lastTime = currentTime
    }
    
    func gameOver() {
        SoundLib.stopBackground(self.spriteBackground)
        self.endScoreLabel.text = String(self.score)
        self.ending = true
        if self.score > UserData.UserSettings.bestScore {
            UserData.UserSettings.bestScore = self.score
            UserData.setUserData("BestScore", value: self.score)
            self.bestScoreLabel.text = String(self.score)
        }
        
        if UserData.UserSettings.gameCenter != nil {
            UserData.UserSettings.gameCenter!.setGameCenterScore(self.score)
        }
        
        self.animating = false
        
        if UserData.UserSettings.soundEnabled {
            self.endPopup.runAction(SKAction.group([self.getActionForShowPopup(), SoundLib.getSoundAction(SoundNames.Lose)]))
        }
        else {
            self.endPopup.runAction(self.getActionForShowPopup())
        }
    }
    
    func clearBlocks() -> Bool {
        let destroyBlocks = self.tetrixGrid!.clearBlocks()
        
        if destroyBlocks.0 == 0 {
            // Not clear
            self.consecutiveClear = 0
            ++self.consecutiveUnclear
        }
        else {
            self.consecutiveUnclear = 0
            ++self.consecutiveClear
        }
        
        self.score += ((destroyBlocks.0 * (destroyBlocks.0 - 1) / 2 * 5) + destroyBlocks.1)
        
        self.scoreLabel.text = String(self.score)
        
        // Check bonus
        if destroyBlocks.0 == 2 {
            // New fill in bonus
            ++self.fillInCount
            self.fillInCountLabel.text = "x" + String(self.fillInCount)
            self.spriteFillIn.colorBlendFactor = 0
            self.fillInCountLabel.colorBlendFactor = 0
            self.animateBonus(self.spriteFillInBonus, closure: { () -> () in
            })
            
        }
        else if destroyBlocks.0 == 3 {
            // New rotate bonus
            ++self.rotateCount
            self.rotateCountLabel.text = String(self.rotateCount)
            self.spriteRotate.colorBlendFactor = 0
            self.rotateCountLabel.colorBlendFactor = 0
            self.animateBonus(self.spriteRotateBonus, closure: { () -> () in
            })
        }
        else if destroyBlocks.0 > 3 {
            // New bomb bonus
            ++self.bombCount
            self.bombCountLabel.text = String(self.bombCount)
            self.spriteBomb.colorBlendFactor = 0
            self.bombCountLabel.colorBlendFactor = 0
            self.animateBonus(self.spriteBombBonus, closure: { () -> () in
            })
        }
        
        let lose = self.tetrixGrid!.update(self.currentTime, score: self.score, consecutiveClear: self.consecutiveClear, consecutiveUnclear: self.consecutiveUnclear)
        if lose.1 {
            self.consecutiveClear = 0
            self.consecutiveUnclear = 0
        }
        
        // Check lost situation
        if self.tetrixGrid!.isFull() || (self.bombCount == 0 && self.fillInCount == 0 && self.rotateCount == 0 && lose.0) {
            self.gameOver()
            return false
        }
        
        return true
    }
    
    typealias Closure = () -> ()
    private var closures = [String: Closure]()
    
    func delayed(delay: Double, name: String, closure: Closure) {
        closures[name] = closure
        dispatch_after(
            dispatch_time(
                DISPATCH_TIME_NOW,
                Int64(delay * Double(NSEC_PER_SEC))
            ),
            dispatch_get_main_queue()) {
                if let closure = self.closures[name] {
                    closure()
                    self.closures[name] = nil
                }
        }
    }
    
    func cancelDelayed(name: String) {
        closures[name] = nil
    }
    
    func getActionForShowPopup() -> SKAction {
        let scale = SKAction.scaleTo(1, duration: 0.2)
        let fadeIn = SKAction.fadeAlphaTo(1, duration: 0.2)
        let group = SKAction.group([scale, fadeIn])

        return group
    }
    
    func getActionForHidePopup() -> SKAction {
        let scale = SKAction.scaleTo(0, duration: 0.2)
        let fadeOut = SKAction.fadeAlphaTo(0, duration: 0.2)
        let group = SKAction.group([scale, fadeOut])

        return group
    }
    
    func getActionForDropBlock(x: CGFloat, y: CGFloat, sX: CGFloat, sY: CGFloat, failed: Bool, remove: Bool, closure: Closure) -> SKAction {
        let move = SKAction.moveTo(CGPoint(x: x, y: y), duration: 0.2) //, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 0)
        let scaleX = SKAction.scaleXTo(sX, duration: 0.2) //, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 0)
        let scaleY = SKAction.scaleYTo(sY, duration: 0.2) //, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 0)
        let removeAction = remove ? SKAction.removeFromParent() : SKAction.waitForDuration(0.1)
        let action = SKAction.runBlock {
            closure()
        }

        if UserData.UserSettings.soundEnabled {
            if failed {
                return SKAction.sequence([SKAction.group([move, scaleX, scaleY, SoundLib.getSoundAction(SoundNames.FailedDrop)]), action, removeAction])
            }
            else {
                return SKAction.sequence([SKAction.group([move, scaleX, scaleY, SoundLib.getSoundAction(SoundNames.Drop)]), action, removeAction])
            }
        }
        else {
            return SKAction.sequence([SKAction.group([move, scaleX, scaleY]), action, removeAction])
        }
    }
    
    func getActionForPickBlock(deltaX: CGFloat, deltaY: CGFloat, scale: CGFloat, closure: Closure) -> SKAction {
        let move = SKAction.moveBy(CGVector(dx: deltaX, dy: deltaY), duration: 0.2) //, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 0)
        let scale = SKAction.scaleTo(scale, duration: 0.2) //, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 0)
        let action = SKAction.runBlock {
            closure()
        }
        
        if UserData.UserSettings.soundEnabled {
            return SKAction.sequence([SKAction.group([move, scale, SoundLib.getSoundAction(SoundNames.Touch)]), action])
        }
        else {
            return SKAction.sequence([SKAction.group([move, scale]), action])
        }
    }
    
    func getActionForBonus(closure: Closure) -> SKAction {
        let scaleX = SKAction.scaleXTo(1, duration: 0.2) //, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 0)
        let scaleY = SKAction.scaleYTo(1, duration: 0.2) //, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 0)
        let fadeIn = SKAction.fadeInWithDuration(0.2) //, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 0)
        let action = SKAction.runBlock {
            closure()
        }
        
        return SKAction.sequence([SKAction.group([scaleX, scaleY, fadeIn]), action]);
    }
    
    func getActionForBonusOut(closure: Closure) -> SKAction {
        let scaleX = SKAction.scaleXTo(4, duration: 0.2) //, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 0)
        let scaleY = SKAction.scaleYTo(4, duration: 0.2) //, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 0)
        let fadeOut = SKAction.fadeOutWithDuration(0.2) //, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 0)
        let action = SKAction.runBlock {
            closure()
        }
        
        return SKAction.sequence([SKAction.group([scaleX, scaleY, fadeOut]), action]);
    }
    
    func animateBonus(bonus: SKNode, closure: Closure) {
        // Set starting value
        bonus.xScale = 4
        bonus.yScale = 4
        bonus.alpha = 0
        bonus.runAction(getActionForBonus(closure))
    }
    
    func animateBonusOut(bonus: SKNode, closure: Closure) {
        bonus.runAction(SKAction.sequence([getActionForBonusOut(closure), getActionForBonus({ () -> () in
        })]))
    }
    
    func getActionForRotateBlock() -> SKAction {
        let delay = NSTimeInterval(CGFloat(arc4random_uniform(10)) / CGFloat(100) * 2)
        let moveUp = SKAction.moveBy(CGVector(dx: 0, dy: 30), duration: 0.5, delay: delay, usingSpringWithDamping: 0.2, initialSpringVelocity: 0)
        let sleep = SKAction.waitForDuration(0.1)
        let moveDown = SKAction.moveBy(CGVector(dx: 0, dy: -30), duration: 0.5, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 0)
        let wait = SKAction.waitForDuration(0.5)
        return SKAction.repeatActionForever(SKAction.sequence([moveUp, sleep, moveDown, wait]))
    }
    
    func shareToFacebook(text: String, image: UIImage?) {
        let currentViewController: UIViewController = UIApplication.sharedApplication().keyWindow!.rootViewController!
        if SLComposeViewController.isAvailableForServiceType(SLServiceTypeFacebook){
            var facebookSheet:SLComposeViewController = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
            facebookSheet.setInitialText(text)
            facebookSheet.addImage(image)
            currentViewController.presentViewController(facebookSheet, animated: true, completion: nil)
        } else {
            var alert = UIAlertController(title: "TeBreak", message: "Please login to a Facebook account to share.", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
            currentViewController.presentViewController(alert, animated: true, completion: nil)
        }
    }
    
    func generateScoreImage(scr: Int) -> UIImage! {
        let baseImage = UIImage(named: "Icon_Score")
        UIGraphicsBeginImageContext(baseImage!.size)
        baseImage!.drawInRect(CGRect(x: 0, y: 0, width: baseImage!.size.width, height: baseImage!.size.height))
        let rect = CGRect(x: 28, y: scr <= 999 ? 50 : (scr <= 9999 ? 60 : 70), width: 101, height: 60)
        let font = UIFont(name: "ChalkboardSE-Bold", size: scr <= 999 ? 50 : (scr <= 9999 ? 40 : 30))
        let textStyle = NSMutableParagraphStyle.defaultParagraphStyle().mutableCopy() as NSMutableParagraphStyle
        textStyle.alignment = NSTextAlignment.Center
        if let actualFont = font {
            let textFontAttributes = [
                NSFontAttributeName: actualFont,
                NSForegroundColorAttributeName: UIColor(red: 79/255.0, green: 150/255.0, blue: 45/255.0, alpha: 1),
                NSParagraphStyleAttributeName: textStyle
            ]
            NSString(string: String(scr)).drawInRect(rect, withAttributes: textFontAttributes)
        }
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage;
    }
}
