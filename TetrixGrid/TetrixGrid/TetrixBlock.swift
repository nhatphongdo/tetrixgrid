//
//  TetrixBlock.swift
//  TetrixGrid
//
//  Created by Do Nhat Phong on 1/23/15.
//  Copyright (c) 2015 VietDev Corporation. All rights reserved.
//

import Foundation
import SpriteKit

enum BlockCode: Int {
    case Empty = 0
    case Selection = 255
    case Stone = 254
    case Block01 = 1
    case Block02 = 2
    case Block03 = 3
    case Block04 = 4
    case Block05 = 5
    case Block06 = 6
    case Block07 = 7
    case EnemyBlock01 = -1
    case EnemyBlock02 = -2
    case EnemyBlock03 = -3
    case EnemyBlock04 = -4
    case EnemyBlock05 = -5
    case EnemyBlock06 = -6
    case EnemyBlock07 = -7
    case FillIn = 253
    case Bomb = 252
    
    static func random() -> BlockCode {
        return BlockCode(rawValue: Int(arc4random_uniform(7)) + 1)!
    }
}

enum Orientation: Int {
    case Zero = 0
    case Ninety
    case OneEighty
    case TwoSeventy
    
    static func random() -> Orientation {
        return Orientation(rawValue: Int(arc4random_uniform(4)))!
    }
    
    static func rotate(orientation: Orientation, clockwise: Bool) -> Orientation {
        var rotated = orientation.rawValue + (clockwise ? 1 : -1)
        if rotated > Orientation.TwoSeventy.rawValue {
            rotated = Orientation.Zero.rawValue
        }
        else if rotated < 0 {
            rotated = Orientation.TwoSeventy.rawValue
        }
        
        return Orientation(rawValue:rotated)!
    }
}

enum BlockType: Int {
    case Dot = 0
    case Line2
    case Line3
    case Line4
    case Line5
    case L2
    case L3
    case L4
    case L5
    case ReversedL3
    case ReversedL4
    case Square2
    case Square3
    case Z1
    case Z2
    case U
    case Plus
    case T1
    case T2
    case T3
    
    static func random() -> BlockType {
        return BlockType(rawValue: Int(arc4random_uniform(20)))!
    }
}


class TetrixBlock : NSObject {
    
    let BlockSpriteNames = ["Block01", "Block02", "Block03", "Block04", "Block05", "Block06", "Block07", "Block08", "Block09", "Block10", "Icon_FillIn", "Overlay_Bomb"]
    
    let gap = CGFloat(1)
    
    var blockCode = BlockCode.Empty
    var orientation = Orientation.Zero
    var blockType = BlockType.Dot
    var cellSize: CGFloat = 50
    var overlay: Bool = false
    var overlayColor: UIColor = UIColor.redColor()
    
    var spriteBlockTexture: SKTexture? = nil
    var BlockNode = SKSpriteNode()
    
    var x: CGFloat = 0
    var y: CGFloat = 0
    
    var blockData = [[Int]]()
    
    init(type: BlockType, block: BlockCode, orientation: Orientation, size: CGFloat = 50) {
        super.init()

        self.blockType = type
        self.blockCode = block
        self.orientation = orientation
        self.spriteBlockTexture = SKTexture(imageNamed: getBlockSpriteName(block))
        self.blockData = getBlockData(type, orientation: orientation)
        self.BlockNode.anchorPoint = CGPoint(x: 0, y: 1)

        var width = CGFloat(0)
        var x = CGFloat(0), y = CGFloat(0)
        for (rIndex, row) in enumerate(self.blockData) {
            for (cIndex, col) in enumerate(row) {
                if col == 1 {
                    var node = SKSpriteNode(texture: self.spriteBlockTexture)
                    node.xScale = self.spriteBlockTexture!.size().width / cellSize
                    node.yScale = self.spriteBlockTexture!.size().height / cellSize
                    node.name = "\(rIndex)_\(cIndex)"
                    node.anchorPoint = CGPoint(x: 0, y: 1)
                    node.position = CGPoint(x: x, y: y)
                    self.BlockNode.addChild(node)
                }
                x += cellSize + gap
            }
            
            width = max(width, x)
            x = 0
            y -= (cellSize + gap)
        }
        
        self.BlockNode.size.width = width
        self.BlockNode.size.height = -y
    }
    
    func redraw() {
        self.spriteBlockTexture = SKTexture(imageNamed: self.getBlockSpriteName(self.blockCode))
        for child in self.BlockNode.children {
            if let node = child as? SKSpriteNode {
                node.texture = self.spriteBlockTexture
            }
        }
        
        if overlay {
            for child in self.BlockNode.children as [SKSpriteNode] {
                child.colorBlendFactor = 0.3
                child.color = overlayColor
            }
        }
        else {
            for child in self.BlockNode.children as [SKSpriteNode] {
                child.colorBlendFactor = 0.0
            }
        }
    }
    
    func recreate() {
        self.spriteBlockTexture = SKTexture(imageNamed: getBlockSpriteName(self.blockCode))
        self.blockData = getBlockData(self.blockType, orientation: self.orientation)
        self.BlockNode.anchorPoint = CGPoint(x: 0, y: 1)
        
        for child in self.BlockNode.children {
            child.removeFromParent()
        }
        self.BlockNode.removeAllChildren()
        
        var width = CGFloat(0)
        var x = CGFloat(0), y = CGFloat(0)
        for (rIndex, row) in enumerate(self.blockData) {
            for (cIndex, col) in enumerate(row) {
                if col == 1 {
                    var node = SKSpriteNode(texture: self.spriteBlockTexture)
                    node.xScale = self.spriteBlockTexture!.size().width / cellSize
                    node.yScale = self.spriteBlockTexture!.size().height / cellSize
                    node.name = "\(rIndex)_\(cIndex)"
                    node.anchorPoint = CGPoint(x: 0, y: 1)
                    node.position = CGPoint(x: x, y: y)
                    self.BlockNode.addChild(node)
                }
                x += cellSize + gap
            }
            
            width = max(width, x)
            x = 0
            y -= (cellSize + gap)
        }
        
        self.BlockNode.size.width = width
        self.BlockNode.size.height = -y
    }
    
    func count() -> Int {
        var count = 0
        for (rIndex, row) in enumerate(self.blockData) {
            for (cIndex, col) in enumerate(row) {
                if col == 1 {
                    ++count
                }
            }
        }
        
        return count
    }
    
    class func generateRandomly() -> TetrixBlock {
        var block = TetrixBlock(type: BlockType.random(), block: BlockCode.random(), orientation: Orientation.random())
        return block
    }
    
    func getBlockSpriteName(code: BlockCode) -> String {
        switch code {
        case BlockCode.Empty:
            return BlockSpriteNames[0]
        case BlockCode.Selection:
            return BlockSpriteNames[8]
        case BlockCode.Stone:
            return BlockSpriteNames[9]
        case BlockCode.Block01:
            return BlockSpriteNames[1]
        case BlockCode.Block02:
            return BlockSpriteNames[2]
        case BlockCode.Block03:
            return BlockSpriteNames[3]
        case BlockCode.Block04:
            return BlockSpriteNames[4]
        case BlockCode.Block05:
            return BlockSpriteNames[5]
        case BlockCode.Block06:
            return BlockSpriteNames[6]
        case BlockCode.Block07:
            return BlockSpriteNames[7]
        case BlockCode.FillIn:
            return BlockSpriteNames[10]
        case BlockCode.Bomb:
            return BlockSpriteNames[11]
        default:
            // Enemy block
            return BlockSpriteNames[9]
        }
    }
    
    func getBlockData(type: BlockType, orientation: Orientation) -> [[Int]] {
        switch type {
        case BlockType.Dot:
            return [[ 1 ]]
        case BlockType.Line2:
            switch orientation {
            case Orientation.Zero, Orientation.OneEighty:
                return [[ 1, 1 ]]
            default:
                return [[ 1 ],
                        [ 1 ]]
            }
        case BlockType.Line3:
            switch orientation {
            case Orientation.Zero, Orientation.OneEighty:
                return [[ 1, 1, 1 ]]
            default:
                return [[ 1 ],
                        [ 1 ],
                        [ 1 ]]
            }
        case BlockType.Line4:
            switch orientation {
            case Orientation.Zero, Orientation.OneEighty:
                return [[ 1, 1, 1, 1 ]]
            default:
                return [[ 1 ],
                        [ 1 ],
                        [ 1 ],
                        [ 1 ]]
            }
        case BlockType.Line5:
            switch orientation {
            case Orientation.Zero, Orientation.OneEighty:
                return [[ 1, 1, 1, 1, 1 ]]
            default:
                return [[ 1 ],
                        [ 1 ],
                        [ 1 ],
                        [ 1 ],
                        [ 1 ]]
            }
        case BlockType.L2:
            switch orientation {
            case Orientation.Zero:
                return [[ 1, 0 ],
                        [ 1, 1 ]]
            case Orientation.Ninety:
                return [[ 1, 1 ],
                        [ 1, 0 ]]
            case Orientation.OneEighty:
                return [[ 1, 1 ],
                        [ 0, 1 ]]
            default:
                return [[ 0, 1 ],
                        [ 1, 1 ]]
            }
        case BlockType.L3:
            switch orientation {
            case Orientation.Zero:
                return [[ 1, 0, 0 ],
                        [ 1, 1, 1 ]]
            case Orientation.Ninety:
                return [[ 1, 1 ],
                        [ 1, 0 ],
                        [ 1, 0 ]]
            case Orientation.OneEighty:
                return [[ 1, 1, 1 ],
                        [ 0, 0, 1 ]]
            default:
                return [[ 0, 1 ],
                        [ 0, 1 ],
                        [ 1, 1 ]]
            }
        case BlockType.L4:
            switch orientation {
            case Orientation.Zero:
                return [[ 1, 0, 0, 0 ],
                        [ 1, 1, 1, 1 ]]
            case Orientation.Ninety:
                return [[ 1, 1 ],
                        [ 1, 0 ],
                        [ 1, 0 ],
                        [ 1, 0 ]]
            case Orientation.OneEighty:
                return [[ 1, 1, 1, 1 ],
                        [ 0, 0, 0, 1 ]]
            default:
                return [[ 0, 1 ],
                        [ 0, 1 ],
                        [ 0, 1 ],
                        [ 1, 1 ]]
            }
        case BlockType.L5:
            switch orientation {
            case Orientation.Zero:
                return [[ 1, 0, 0 ],
                        [ 1, 0, 0 ],
                        [ 1, 1, 1 ]]
            case Orientation.Ninety:
                return [[ 1, 1, 1 ],
                        [ 1, 0, 0 ],
                        [ 1, 0, 0 ]]
            case Orientation.OneEighty:
                return [[ 1, 1, 1 ],
                        [ 0, 0, 1 ],
                        [ 0, 0, 1 ]]
            default:
                return [[ 0, 0, 1 ],
                        [ 0, 0, 1 ],
                        [ 1, 1, 1 ]]
            }
        case BlockType.ReversedL3:
            switch orientation {
            case Orientation.Zero:
                return [[ 0, 0, 1 ],
                        [ 1, 1, 1 ]]
            case Orientation.Ninety:
                return [[ 1, 0 ],
                        [ 1, 0 ],
                        [ 1, 1 ]]
            case Orientation.OneEighty:
                return [[ 1, 1, 1 ],
                        [ 1, 0, 0 ]]
            default:
                return [[ 1, 1 ],
                        [ 0, 1 ],
                        [ 0, 1 ]]
            }
        case BlockType.ReversedL4:
            switch orientation {
            case Orientation.Zero:
                return [[ 0, 0, 0, 1 ],
                        [ 1, 1, 1, 1 ]]
            case Orientation.Ninety:
                return [[ 1, 0 ],
                        [ 1, 0 ],
                        [ 1, 0 ],
                        [ 1, 1 ]]
            case Orientation.OneEighty:
                return [[ 1, 1, 1, 1 ],
                        [ 1, 0, 0, 0 ]]
            default:
                return [[ 1, 1 ],
                        [ 0, 1 ],
                        [ 0, 1 ],
                        [ 0, 1 ]]
            }
        case BlockType.Square2:
            return [[ 1, 1 ],
                    [ 1, 1 ]]
        case BlockType.Square3:
            return [[ 1, 1, 1 ],
                    [ 1, 1, 1 ],
                    [ 1, 1, 1 ]]
        case BlockType.Z1:
            switch orientation {
            case Orientation.Zero, Orientation.OneEighty:
                return [[ 1, 1, 0 ],
                        [ 0, 1, 1 ]]
            default:
                return [[ 0, 1 ],
                        [ 1, 1 ],
                        [ 1, 0 ]]
            }
        case BlockType.Z2:
            switch orientation {
            case Orientation.Zero, Orientation.OneEighty:
                return [[ 0, 1, 1 ],
                        [ 1, 1, 0 ]]
            default:
                return [[ 1, 0 ],
                        [ 1, 1 ],
                        [ 0, 1 ]]
            }
        case BlockType.U:
            switch orientation {
            case Orientation.Zero:
                return [[ 1, 0, 1 ],
                        [ 1, 1, 1 ]]
            case Orientation.Ninety:
                return [[ 1, 1 ],
                        [ 1, 0 ],
                        [ 1, 1 ]]
            case Orientation.OneEighty:
                return [[ 1, 1, 1 ],
                        [ 1, 0, 1 ]]
            default:
                return [[ 1, 1 ],
                        [ 0, 1 ],
                        [ 1, 1 ]]
            }
        case BlockType.Plus:
            return [[ 0, 1, 0 ],
                    [ 1, 1, 1 ],
                    [ 0, 1, 0 ]]
        case BlockType.T1:
            switch orientation {
            case Orientation.Zero:
                return [[ 0, 1, 0 ],
                        [ 1, 1, 1 ]]
            case Orientation.Ninety:
                return [[ 1, 0 ],
                        [ 1, 1 ],
                        [ 1, 0 ]]
            case Orientation.OneEighty:
                return [[ 1, 1, 1 ],
                        [ 0, 1, 0 ]]
            default:
                return [[ 0, 1 ],
                        [ 1, 1 ],
                        [ 0, 1 ]]
            }
        case BlockType.T2:
            switch orientation {
            case Orientation.Zero:
                return [[ 0, 1, 0, 1, 0 ],
                        [ 1, 1, 1, 1, 1 ]]
            case Orientation.Ninety:
                return [[ 1, 0 ],
                        [ 1, 1 ],
                        [ 1, 0 ],
                        [ 1, 1 ],
                        [ 1, 0 ]]
            case Orientation.OneEighty:
                return [[ 1, 1, 1, 1, 1 ],
                        [ 0, 1, 0, 1, 0 ]]
            default:
                return [[ 0, 1 ],
                        [ 1, 1 ],
                        [ 0, 1 ],
                        [ 1, 1 ],
                        [ 0, 1 ]]
            }
        case BlockType.T3:
            switch orientation {
            case Orientation.Zero:
                return [[ 0, 1, 0 ],
                        [ 0, 1, 0 ],
                        [ 1, 1, 1 ]]
            case Orientation.Ninety:
                return [[ 1, 0, 0 ],
                        [ 1, 1, 1 ],
                        [ 1, 0, 0 ]]
            case Orientation.OneEighty:
                return [[ 1, 1, 1 ],
                        [ 0, 1, 0 ],
                        [ 0, 1, 0 ]]
            default:
                return [[ 0, 0, 1 ],
                        [ 1, 1, 1 ],
                        [ 0, 0, 1 ]]
            }
        default:
            return [[ 0 ]]
        }
    }
}