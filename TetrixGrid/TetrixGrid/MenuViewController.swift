//
//  MenuViewController.swift
//  TetrixGrid
//
//  Created by Do Nhat Phong on 1/19/15.
//  Copyright (c) 2015 VietDev Corporation. All rights reserved.
//

import UIKit
import SpriteKit
import GoogleMobileAds

class MenuViewController: UIViewController, GADInterstitialDelegate {
    
    @IBOutlet var googleTopAdView: GADBannerView!
    @IBOutlet var googleBottomAdView: GADBannerView!
    var googleAdInterstitial: GADInterstitial!
    
    var closeAdFunc: (() -> ())? = nil
    
    var scene: MenuScene? = nil
    var gameScene: GameScene? = nil
    
    var gameCenter: GameCenter? = nil
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        let size = CGSizeMake(640, 1136)
        scene = MenuScene(size: size)
        
        /* Set the scale mode to scale to fit the window */
        scene!.scaleMode = .AspectFit
        scene!.viewController = self

        gameScene = GameScene(size: size)
        
        /* Set the scale mode to scale to fit the window */
        gameScene!.scaleMode = .AspectFit
        gameScene!.viewController = self
        
        scene!.gameScene = gameScene
        gameScene!.menuScene = scene
        
        // Load user settings
        UserData.loadUserSettings()
        gameCenter = GameCenter()

        gameCenter!.loginToGameCenter(self, avatarSize: scene!.AvatarSize, authenFinished: { () -> () in
            self.scene!.setUsername(UserData.UserSettings.userName)
            }) { () -> () in
                self.scene!.spriteAvatar.texture = UserData.UserSettings.avatar
        }
        UserData.UserSettings.gameCenter = gameCenter

        let skView = self.view as SKView
        
        /* Sprite Kit applies additional optimizations to improve rendering performance */
        skView.ignoresSiblingOrder = true
        
        skView.presentScene(scene)
        
        self.googleTopAdView.adUnitID = "ca-app-pub-1266895752139095/3749328363"
        self.googleTopAdView.rootViewController = self
        self.googleTopAdView.loadRequest(GADRequest())
        self.hideTopAd()

        self.googleBottomAdView.adUnitID = "ca-app-pub-1266895752139095/3749328363"
        self.googleBottomAdView.rootViewController = self
        self.googleBottomAdView.loadRequest(GADRequest())
//        self.hideBottomAd()
        
        self.googleAdInterstitial = self.createAndLoadInterstitial()
    }
        
    override func shouldAutorotate() -> Bool {
        return true
    }
    
    override func supportedInterfaceOrientations() -> Int {
        if UIDevice.currentDevice().userInterfaceIdiom == .Phone {
            return Int(UIInterfaceOrientationMask.AllButUpsideDown.rawValue)
        } else {
            return Int(UIInterfaceOrientationMask.All.rawValue)
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Release any cached data, images, etc that aren't in use.
        println("Memory low")
    }
    
    override func prefersStatusBarHidden() -> Bool {
        return true
    }
    
    func createAndLoadInterstitial() -> GADInterstitial! {
        var adInterstitial = GADInterstitial(adUnitID: "ca-app-pub-1266895752139095/3749328363")
        adInterstitial.delegate = self
        adInterstitial.loadRequest(GADRequest())
        
        return adInterstitial
    }
    
    func interstitialDidDismissScreen(interstitial:GADInterstitial) {
        self.googleAdInterstitial = self.createAndLoadInterstitial()
        if self.closeAdFunc != nil {
            self.closeAdFunc!()
        }
    }
    
    func showInterstitialAd(closeFunc: () -> ()) {
        if self.googleAdInterstitial.isReady {
            self.googleAdInterstitial.presentFromRootViewController(self)
        }
        
        self.closeAdFunc = closeFunc
    }
    
    func showTopAd() {
        self.googleTopAdView.hidden = false
    }
    
    func hideTopAd() {
        self.googleTopAdView.hidden = true
    }
    
    func showBottomAd() {
        self.googleBottomAdView.hidden = false
    }
    
    func hideBottomAd() {
        self.googleBottomAdView.hidden = true
    }
}
