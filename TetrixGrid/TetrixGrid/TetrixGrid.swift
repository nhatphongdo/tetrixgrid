//
//  TetrixGrid.swift
//  TetrixGrid
//
//  Created by Do Nhat Phong on 1/23/15.
//  Copyright (c) 2015 VietDev Corporation. All rights reserved.
//

import Foundation
import UIKit
import SpriteKit

enum GameModes: Int {
    case Normal
    case Timed
    case Multiplayer
}

struct Difficulty {
    var StonesEachTime: Int = 3
    var StonesGenerateTime: Double = 30 // 30 seconds to generate stones
    var InitialTimer: Double = 30 // 30 seconds
    var ClearBonusTime: Double = 5 // 5 seconds
    var AvailableBlockTypes: [BlockType] = [BlockType]()
}

struct GameSettings {
    var Mode: GameModes = GameModes.Normal
    var HorizontalBlockCount: Int = 10
    var VerticalBlockCount: Int = 10
    var TwoPlayerHorizontalBlockCount: Int = 10
    var TwoPlayerVerticalBlockCount: Int = 20
    var NextBlockCount: Int = 3
    var HorizontalNextBlock: Bool = true
    var GridBlockSize: CGFloat = 49
    var PlayableLeft: Int = 0
    var PlayableRight: Int = 0
    var PlayableTop: Int = 0
    var PlayableBottom: Int = 0
    var BombCount: Int = 0
    var RotateCount: Int = 0
    var FillInCount: Int = 0
    var DifficultyScores: [Int] = [200, 400, 600, 900, 1200]
}


class TetrixGrid : NSObject {

    let gap = CGFloat(1)
    
    var grid: [[BlockCode]]

    var Settings = GameSettings()
    
    var GridNode = SKSpriteNode()

    var blocks: [[TetrixBlock?]]
    
    var explodedBlocks = [TetrixBlock]()
    
    var nextBlocks = [TetrixBlock]()
    var NextBlockNode = SKSpriteNode()
    
    var difficulties = [Difficulty]()
    var currentDifficulty: Int = 0
    
    var lastTime: Double = 0
    
    init(settings: GameSettings) {
        
        self.Settings = settings
        
        // Init game grid
        let hCount = settings.Mode == GameModes.Multiplayer ? settings.TwoPlayerHorizontalBlockCount : settings.HorizontalBlockCount
        let vCount = settings.Mode == GameModes.Multiplayer ? settings.TwoPlayerVerticalBlockCount : settings.VerticalBlockCount
        grid = [[BlockCode]](count: vCount, repeatedValue: [BlockCode](count: hCount, repeatedValue: BlockCode.Empty))
        
        blocks = [[TetrixBlock?]](count: vCount, repeatedValue: [TetrixBlock?](count: hCount, repeatedValue: nil))
        
        super.init()

        GridNode.anchorPoint = CGPoint(x: 0, y: 1)
        
        NextBlockNode.anchorPoint = CGPoint(x: 0, y: 0)

        initDifficulties()

        generateNextBlocks()
        
        draw()
    }
    
    func reset() {
        lastTime = 0
        currentDifficulty = 0
        
        for (rIndex, row) in enumerate(grid) {
            for (cIndex, col) in enumerate(row) {
                grid[rIndex][cIndex] = BlockCode.Empty
            }
        }
        
        redraw()
        
        nextBlocks.removeAll(keepCapacity: true)
        generateNextBlocks()
    }
    
    func draw() {
        GridNode.removeAllChildren()
        
        var width = CGFloat(0)
        var x = CGFloat(0), y = CGFloat(0)
        for (rIndex, row) in enumerate(grid) {
            var height = CGFloat(0)
            for (cIndex, col) in enumerate(row) {
                var block = TetrixBlock(type: BlockType.Dot, block: col, orientation: Orientation.Zero, size: Settings.GridBlockSize)
                block.BlockNode.position = CGPoint(x: x, y: y)
                GridNode.addChild(block.BlockNode)
                
                blocks[rIndex][cIndex] = block
                
                x += block.BlockNode.size.width + gap
                height = max(height, block.BlockNode.size.height)
            }
            
            width = max(width, x)
            x = 0
            y -= height - gap
        }
        
        GridNode.size.width = width
        GridNode.size.height = -y
        
        // Draw next blocks
        redrawNextBlocks(true)
    }
    
    func redraw() {
        for (rIndex, row) in enumerate(blocks) {
            for (cIndex, col) in enumerate(row) {
                col!.overlay = false
                col!.blockCode = grid[rIndex][cIndex]
                col!.redraw()
            }
        }
    }
    
    func redrawNextBlocks(animate: Bool) {
        NextBlockNode.removeAllChildren()
        
        var space = CGFloat(40)
        var x = CGFloat(0)
        var y = CGFloat(0)
        var width = CGFloat(0)
        var height = CGFloat(0)
        for block in nextBlocks {
            if Settings.HorizontalNextBlock {
                height = max(height, block.BlockNode.size.height)
                width += block.BlockNode.size.width
            }
            else {
                width = max(width, block.BlockNode.size.width)
                height += block.BlockNode.size.height
            }
        }
        
        if Settings.HorizontalNextBlock {
            space = CGFloat(640 - width) / CGFloat(nextBlocks.count)
            width = CGFloat(0)
        }
        else {
            space = CGFloat(640 - width) / CGFloat(nextBlocks.count)
            height = CGFloat(0)
        }
        
        for (index, block) in enumerate(nextBlocks) {
            block.BlockNode.zPosition = 99
            
            if Settings.HorizontalNextBlock {
                block.x = x
                block.y = -(height - block.BlockNode.size.height)
                block.BlockNode.position = CGPoint(x: block.x + (animate ? 640 : 0), y: block.y) // Initial value, out of scene
                if animate {
                    block.BlockNode.runAction(getActionForNextBlocks(block.x, y: block.y, delay: 0.2 * CGFloat(index)))
                }
                x += block.BlockNode.size.width + space
                width = x - space
            }
            else {
                block.x = x
                block.y = y
                block.BlockNode.position = CGPoint(x: block.x + (animate ? 640 : 0), y: block.y)  // Initial value, out of scene
                if animate {
                    block.BlockNode.runAction(getActionForNextBlocks(block.x, y: block.y, delay: 0.2 * CGFloat(index)))
                }
                y += block.BlockNode.size.height + space
                height = y - space
            }
            
            NextBlockNode.addChild(block.BlockNode)
        }
        
        NextBlockNode.size.width = width
        NextBlockNode.size.height = height
    }
    
    func initDifficulties() {
        var easy = Difficulty()
        easy.StonesEachTime = 0
        easy.StonesGenerateTime = 0
        easy.AvailableBlockTypes =
            [
                BlockType.Dot, BlockType.Dot, BlockType.Dot,
                BlockType.Line2, BlockType.Line2, BlockType.Line2,
                BlockType.Line3, BlockType.Line3, BlockType.Line3,
                BlockType.L2, BlockType.L2, BlockType.L2,
                BlockType.L3, BlockType.L3, BlockType.L3,
                BlockType.ReversedL3, BlockType.ReversedL3, BlockType.ReversedL3,
                BlockType.T1, BlockType.T1, BlockType.T1,
                BlockType.Square2, BlockType.Square2, BlockType.Square2,
                BlockType.Z1, BlockType.Z1, BlockType.Z1,
                BlockType.Z2, BlockType.Z2, BlockType.Z2,
                BlockType.Line4, BlockType.Line4,
                BlockType.U, BlockType.U,
                BlockType.L5, BlockType.L5,
                BlockType.Square3
            ]
        self.difficulties.append(easy)
        
        var normal = Difficulty()
        normal.StonesEachTime = 3
        normal.StonesGenerateTime = 5
        normal.AvailableBlockTypes =
            [
                BlockType.Dot, BlockType.Dot, BlockType.Dot,
                BlockType.Line2, BlockType.Line2, BlockType.Line2,
                BlockType.Line3, BlockType.Line3, BlockType.Line3,
                BlockType.L2, BlockType.L2, BlockType.L2,
                BlockType.L3, BlockType.L3, BlockType.L3,
                BlockType.ReversedL3, BlockType.ReversedL3, BlockType.ReversedL3,
                BlockType.T1, BlockType.T1, BlockType.T1,
                BlockType.Square2, BlockType.Square2, BlockType.Square2,
                BlockType.Z1, BlockType.Z1, BlockType.Z1,
                BlockType.Z2, BlockType.Z2, BlockType.Z2,
                BlockType.Line4, BlockType.Line4,
                BlockType.U, BlockType.U,
                BlockType.L5, BlockType.L5,
                BlockType.L4,
                BlockType.ReversedL4,
                BlockType.Line5,
                BlockType.Square3
        ]
        self.difficulties.append(normal)
        
        var hard = Difficulty()
        hard.StonesEachTime = 5
        hard.StonesGenerateTime = 5
        hard.AvailableBlockTypes =
            [
                BlockType.Dot, BlockType.Dot, BlockType.Dot,
                BlockType.Line2, BlockType.Line2, BlockType.Line2,
                BlockType.Line3, BlockType.Line3, BlockType.Line3,
                BlockType.L2, BlockType.L2, BlockType.L2,
                BlockType.L3, BlockType.L3, BlockType.L3,
                BlockType.ReversedL3, BlockType.ReversedL3, BlockType.ReversedL3,
                BlockType.T1, BlockType.T1, BlockType.T1,
                BlockType.Square2, BlockType.Square2, BlockType.Square2,
                BlockType.Z1, BlockType.Z1, BlockType.Z1,
                BlockType.Z2, BlockType.Z2, BlockType.Z2,
                BlockType.Line4, BlockType.Line4, BlockType.Line4,
                BlockType.U, BlockType.U, BlockType.L5,
                BlockType.L4, BlockType.L4,
                BlockType.ReversedL4, BlockType.ReversedL4,
                BlockType.L5, BlockType.L5,
                BlockType.Line5, BlockType.Line5,
                BlockType.Square3, BlockType.Square3,
                BlockType.Plus,
                BlockType.T2
        ]
        self.difficulties.append(hard)
        
        var harder = Difficulty()
        harder.StonesEachTime = 5
        harder.StonesGenerateTime = 3
        harder.AvailableBlockTypes =
            [
                BlockType.Dot, BlockType.Dot, BlockType.Dot,
                BlockType.Line2, BlockType.Line2, BlockType.Line2,
                BlockType.Line3, BlockType.Line3, BlockType.Line3,
                BlockType.L2, BlockType.L2, BlockType.L2,
                BlockType.L3, BlockType.L3, BlockType.L3,
                BlockType.ReversedL3, BlockType.ReversedL3, BlockType.ReversedL3,
                BlockType.T1, BlockType.T1, BlockType.T1,
                BlockType.Square2, BlockType.Square2, BlockType.Square2,
                BlockType.Z1, BlockType.Z1, BlockType.Z1,
                BlockType.Z2, BlockType.Z2, BlockType.Z2,
                BlockType.Line4, BlockType.Line4, BlockType.Line4,
                BlockType.U, BlockType.U, BlockType.L5,
                BlockType.L4, BlockType.L4, BlockType.L4,
                BlockType.ReversedL4, BlockType.ReversedL4, BlockType.ReversedL4,
                BlockType.L5, BlockType.L5, BlockType.L5,
                BlockType.Line5, BlockType.Line5, BlockType.Line5,
                BlockType.Square3, BlockType.Square3,
                BlockType.Plus, BlockType.Plus,
                BlockType.T3, BlockType.T3,
                BlockType.T2
        ]
        self.difficulties.append(harder)
        
        var veryHard = Difficulty()
        veryHard.StonesEachTime = 5
        veryHard.StonesGenerateTime = 3
        veryHard.AvailableBlockTypes =
            [
                BlockType.Dot,
                BlockType.Line2,
                BlockType.Line3,
                BlockType.Line4,
                BlockType.Line5,
                BlockType.L2,
                BlockType.L3,
                BlockType.L4,
                BlockType.L5,
                BlockType.ReversedL3,
                BlockType.ReversedL4,
                BlockType.Square2,
                BlockType.Square3,
                BlockType.T1,
                BlockType.T2,
                BlockType.T3,
                BlockType.Z1,
                BlockType.Z2,
                BlockType.U,
                BlockType.Plus
        ]
        self.difficulties.append(veryHard)
        
        var superHard = Difficulty()
        superHard.StonesEachTime = 7
        superHard.StonesGenerateTime = 3
        superHard.AvailableBlockTypes =
            [
                BlockType.Dot,
                BlockType.Line2,
                BlockType.Line3,
                BlockType.Line4,
                BlockType.Line5, BlockType.Line5,
                BlockType.L2,
                BlockType.L3,
                BlockType.L4, BlockType.L4,
                BlockType.L5, BlockType.L5,
                BlockType.ReversedL3,
                BlockType.ReversedL4, BlockType.ReversedL4,
                BlockType.Square2,
                BlockType.Square3, BlockType.Square3,
                BlockType.T1,
                BlockType.T2, BlockType.T2,
                BlockType.T3, BlockType.T3,
                BlockType.Z1,
                BlockType.Z2,
                BlockType.U, BlockType.U,
                BlockType.Plus, BlockType.Plus
        ]
        self.difficulties.append(superHard)
    }
    
    func findNextBlockInTouch(touch: AnyObject) -> (Int, TetrixBlock?) {
        let location = touch.locationInNode(NextBlockNode)
        
        for (index, block) in enumerate(nextBlocks) {
            if block.BlockNode.containsPoint(location) {
                return (index, block)
            }
        }
        
        return (-1, nil)
    }
    
    func getCoverBlocks(movingBlock: TetrixBlock, touch: AnyObject) -> [(Int, Int)] {
        var result = [(Int, Int)]()
        
        var locationInGrid = touch.locationInNode(GridNode)
        var locationInMovingBlock = touch.locationInNode(movingBlock.BlockNode)
        var movingX = locationInGrid.x - locationInMovingBlock.x
        var movingY = locationInGrid.y - locationInMovingBlock.y
        var xGrid = Int(round(movingX / Settings.GridBlockSize))
        var yGrid = Int(round(-movingY / Settings.GridBlockSize))
        
        for (rIndex, row) in enumerate(movingBlock.blockData) {
            for (cIndex, col) in enumerate(row) {
                if col == 1 {
                    result.append((xGrid + cIndex, yGrid + rIndex))
                }
            }
        }
        
        return result
    }
    
    func highlightBlocksInGrid(movingBlock: TetrixBlock, touch: AnyObject, bombOverlay: Bool) {
        // Find rect that covered by moving block
        let movingRect = CGRect(origin: movingBlock.BlockNode.position, size: movingBlock.BlockNode.size)
        let gridRect = CGRect(origin: GridNode.position, size: GridNode.size)
        
        clearAllHighligh()
        let coveredBlocks = getCoverBlocks(movingBlock, touch: touch)
        for item in coveredBlocks {
            highlight(item.0, y: item.1)
            
            if bombOverlay {
                for row in (item.1 - 1)...(item.1 + 1) {
                    for col in (item.0 - 1)...(item.0 + 1) {
                        if col >= Settings.PlayableLeft && col <= Settings.PlayableRight && row >= Settings.PlayableTop && row <= Settings.PlayableBottom {
                            blocks[row][col]!.overlay = true
                            blocks[row][col]!.redraw()
                        }
                    }
                }
            }
        }
    }
    
    func clearAllHighligh() {
        for (rIndex, row) in enumerate(grid) {
            for (cIndex, col) in enumerate(row) {
                if col == BlockCode.Selection {
                    grid[rIndex][cIndex] = BlockCode.Empty
                }
            }
        }
        
        redraw()
    }
    
    func highlight(x: Int, y: Int) {
        if x >= Settings.PlayableLeft && x <= Settings.PlayableRight && y >= Settings.PlayableTop && y <= Settings.PlayableBottom && grid[y][x] == BlockCode.Empty {
            grid[y][x] = BlockCode.Selection
            redraw()
        }
    }
    
    func releaseBlock(movingBlock: TetrixBlock, touch: AnyObject) -> (Bool, CGFloat, CGFloat) {
        clearAllHighligh()

        let coveredBlocks = getCoverBlocks(movingBlock, touch: touch)
        for item in coveredBlocks {
            if item.0 < Settings.PlayableLeft || item.0 > Settings.PlayableRight || item.1 < Settings.PlayableTop || item.1 > Settings.PlayableBottom || grid[item.1][item.0] != BlockCode.Empty {
                return (false, 0, 0)
            }
        }
        
        var x = Int.max
        var y = Int.max
        for item in coveredBlocks {
            grid[item.1][item.0] = movingBlock.blockCode
            x = min(x, item.0)
            y = min(y, item.1)
        }
        
        return (true, blocks[y][x]!.BlockNode.position.x + GridNode.position.x, blocks[y][x]!.BlockNode.position.y + GridNode.position.y)
    }
    
    func removeNextBlock(index: Int) {
        nextBlocks.removeAtIndex(index)
    }
    
    func getNextBlock(index: Int) -> TetrixBlock {
        return nextBlocks[index]
    }
    
    func isEmptyNextQueue() -> Bool {
        return nextBlocks.isEmpty
    }
    
    func generateNextBlocks() {
        // Find all possible cases
        var possibleCases = [TetrixBlock]()
        
        let availableBlockTypes = getDifficulty(currentDifficulty).AvailableBlockTypes
        
        for blockType in availableBlockTypes {
            findPossibleCase: for orientation in 0...3 {
                let block = TetrixBlock(type: blockType, block: BlockCode.Block01, orientation: Orientation(rawValue: orientation)!, size: Settings.GridBlockSize)
                for (rGrid, rowGrid) in enumerate(grid) {
                    checkGridCell: for (cGrid, colGrid) in enumerate(rowGrid) {
                        for (rIndex, row) in enumerate(block.blockData) {
                            for (cIndex, col) in enumerate(row) {
                                if col == 1 {
                                    let x = cGrid + cIndex
                                    let y = rGrid + rIndex
                                    if x < Settings.PlayableLeft || x > Settings.PlayableRight || y < Settings.PlayableTop || y > Settings.PlayableBottom || grid[y][x] != BlockCode.Empty {
                                        continue checkGridCell
                                    }
                                }
                            }
                        }
                        
                        // Add to list
                        possibleCases.append(block)
                        continue findPossibleCase
                    }
                }
            }
        }
        
        if possibleCases.count > 0 {
            for i in 0...(Settings.NextBlockCount - 1) {
                let index = Int(arc4random_uniform(UInt32(possibleCases.count)))
                let block = TetrixBlock(type: possibleCases[index].blockType, block: BlockCode.random(), orientation: possibleCases[index].orientation, size: Settings.GridBlockSize)
                nextBlocks.append(block)
            }
            
            redrawNextBlocks(true)
        }
    }
    
    func generateStones(count: Int) {
        var possibleCases = [(Int, Int)]()
        
        for (rGrid, rowGrid) in enumerate(grid) {
            for (cGrid, colGrid) in enumerate(rowGrid) {
                if colGrid == BlockCode.Empty {
                    // Add to list
                    possibleCases.append((rGrid, cGrid))
                }
            }
        }
        
        if possibleCases.count > 0 {
            for i in 1...count {
                let index = Int(arc4random_uniform(UInt32(possibleCases.count)))
                grid[possibleCases[index].0][possibleCases[index].1] = BlockCode.Stone
            }
            
            redraw()
        }
    }
    
    func clearBlocks() -> (Int, Int) {
        // Also clear lines that filled by stones not containing this block!!!!
        
        explodedBlocks.removeAll(keepCapacity: false)
        
        var rows = [Int]()
        var columns = [Int]()
        var count: Int = 0
        var lineCount: Int = 0
        
        var animations = [[TetrixBlock]]()
        
        // Clear rows
        for row in Settings.PlayableTop...Settings.PlayableBottom {
            var canClear = true
            for col in Settings.PlayableLeft...Settings.PlayableRight {
                if grid[row][col] == BlockCode.Empty || grid[row][col] == BlockCode.Selection {
                    canClear = false
                }
            }
            
            // Clear
            if canClear {
                ++lineCount
                var animateBlocks = [TetrixBlock]()
                for col in Settings.PlayableLeft...Settings.PlayableRight {
                    ++count
                    
                    var block = TetrixBlock(type: blocks[row][col]!.blockType, block: blocks[row][col]!.blockCode, orientation: blocks[row][col]!.orientation, size: Settings.GridBlockSize)
                    block.BlockNode.name = "\(row)_\(col)"
                    block.BlockNode.position = blocks[row][col]!.BlockNode.position
                    block.BlockNode.zPosition = 99
                    explodedBlocks.append(block)
                    animateBlocks.append(block)
                    
                    rows.append(row)
                }
                
                animations.append(animateBlocks)
            }
        }
        
        // Clear columns
        if Settings.Mode != GameModes.Multiplayer {
            for col in Settings.PlayableLeft...Settings.PlayableRight {
                var canClear = true
                for row in Settings.PlayableTop...Settings.PlayableBottom {
                    if grid[row][col] == BlockCode.Empty || grid[row][col] == BlockCode.Selection{
                        canClear = false
                    }
                }
                
                // Clear
                if canClear {
                    ++lineCount
                    var animateBlocks = [TetrixBlock]()
                    for row in Settings.PlayableTop...Settings.PlayableBottom {
                        ++count

                        if rows.filter({ (T) -> Bool in
                            T == row
                        }).count == 0 {
                            var block = TetrixBlock(type: blocks[row][col]!.blockType, block: blocks[row][col]!.blockCode, orientation: blocks[row][col]!.orientation, size: Settings.GridBlockSize)
                            block.BlockNode.name = "\(row)_\(col)"
                            block.BlockNode.position = blocks[row][col]!.BlockNode.position
                            block.BlockNode.zPosition = 99
                            explodedBlocks.append(block)
                            animateBlocks.append(block)
                            
                            columns.append(col)
                        }
                    }
                    
                    animations.append(animateBlocks)
                }
            }
        }
        
        // Animate exploding
        for animation in animations {
            animateExploding(animation)
        }
        
        for row in rows {
            for col in Settings.PlayableLeft...Settings.PlayableRight {
                grid[row][col] = BlockCode.Empty
            }
        }
        
        for col in columns {
            for row in Settings.PlayableTop...Settings.PlayableBottom {
                grid[row][col] = BlockCode.Empty
            }
        }
        
        redraw()
        
        return (lineCount, count)
    }
    
    func update(time: Double, score: Int, consecutiveClear: Int, consecutiveUnclear: Int) -> (Bool, Bool) {
        // Find difficult level
        let oldCurrentDifficulty = currentDifficulty
        currentDifficulty = -1
        for (index, diffScore) in enumerate(Settings.DifficultyScores) {
            if diffScore < score {
                currentDifficulty = index
                continue
            }
            else {
                break
            }
        }
        ++currentDifficulty
        if currentDifficulty != oldCurrentDifficulty {
            lastTime = time
        }
        
        var generatedBlock = false
        if Double(consecutiveUnclear) >= getDifficulty(currentDifficulty).StonesGenerateTime && getDifficulty(currentDifficulty).StonesEachTime > 0 {
            lastTime = time
            
            // Generate stone blocks
            var stoneCount = getDifficulty(currentDifficulty).StonesEachTime
            stoneCount = Int(arc4random_uniform(UInt32((stoneCount + 1) / 2))) + stoneCount / 2 + 1
            generateStones(stoneCount)
            
            // Check lost situation
            let lose = isLost()
            if lose {
                return (true, true)
            }
            
            generatedBlock = true
        }

        let lose = isLost()
        return (lose, generatedBlock)
    }
    
    func getDifficulty(level: Int) -> Difficulty {
        if level < 0 {
            return difficulties[0]
        }
        
        if level >= difficulties.count {
            return difficulties[difficulties.count - 1]
        }
        
        return difficulties[level]
    }
    
    func isFull() -> Bool {
        var hasEmpty = false
        for (rGrid, rowGrid) in enumerate(grid) {
            for (cGrid, colGrid) in enumerate(rowGrid) {
                if colGrid == BlockCode.Empty {
                    hasEmpty = true
                }
            }
        }
        
        if hasEmpty == false {
            return true
        }
        
        return false
    }
    
    func isLost() -> Bool {
        // No space
        if isFull() {
            return true
        }
        
        if nextBlocks.count == 0 {
            return false
        }
        
        for block in nextBlocks {
            for (rGrid, rowGrid) in enumerate(grid) {
                checkGridCell: for (cGrid, colGrid) in enumerate(rowGrid) {
                    for (rIndex, row) in enumerate(block.blockData) {
                        for (cIndex, col) in enumerate(row) {
                            if col == 1 {
                                let x = cGrid + cIndex
                                let y = rGrid + rIndex
                                if x < Settings.PlayableLeft || x > Settings.PlayableRight || y < Settings.PlayableTop || y > Settings.PlayableBottom || grid[y][x] != BlockCode.Empty {
                                    continue checkGridCell
                                }
                            }
                        }
                    }
                    
                    // Can put this block to grid
                    return false
                }
            }
        }
        
        return true
    }
    
    func getActionForNextBlocks(x: CGFloat, y: CGFloat, delay: CGFloat) -> SKAction {
        let move = SKAction.moveTo(CGPoint(x: x, y: y), duration: 1, delay: NSTimeInterval(delay), usingSpringWithDamping: 0.2, initialSpringVelocity: 0)
        
        if UserData.UserSettings.soundEnabled {
            return SKAction.group([move, SoundLib.getSoundAction(SoundNames.NextBlockAppear)])
        }
        else {
            return move
        }
    }
    
    func animateExploding(blocks: [TetrixBlock]) {
        for (index, block) in enumerate(blocks) {
//            let blockName = block.getBlockSpriteName(block.blockCode)
//            let blowAnimation = SKEmitterNode(fileNamed: "BlowUp" + blockName + ".sks")
//            blowAnimation.position = CGPoint(x: block.BlockNode.position.x + block.BlockNode.size.width / 2, y: block.BlockNode.position.y - block.BlockNode.size.height / 2)
//            blowAnimation.zPosition = 99
//
//            GridNode.addChild(blowAnimation)
            GridNode.addChild(block.BlockNode)
            block.BlockNode.runAction(getActionForDestroyBlocks(index, x: block.BlockNode.position.x + block.BlockNode.size.width / 2, y: block.BlockNode.position.y - block.BlockNode.size.height / 2, { () -> () in
                block.BlockNode.removeFromParent()
            }))
        }
        
        if UserData.UserSettings.soundEnabled && explodedBlocks.count > 0 {
            GridNode.runAction(SoundLib.getSoundAction(SoundNames.Clear))
        }
    }
    
    func getActionForDestroyBlocks(index: Int, x: CGFloat, y: CGFloat, closure: () -> ()) -> SKAction {
        let duration = NSTimeInterval(arc4random_uniform(2)) + 2
//        let radius = CGFloat(UInt(arc4random_uniform(200) + 100))
//        var startAngle = CGFloat(M_PI)
//        var endAngle = startAngle * 2
//        let goLeft = arc4random_uniform(100) % 2 == 0
//        if goLeft {
//            endAngle = startAngle
//            startAngle = 0
//        }
//        let archPath = UIBezierPath(arcCenter: CGPoint(x: x, y: y), radius: radius, startAngle: startAngle, endAngle: endAngle, clockwise: goLeft)
//        let followPath = SKAction.followPath(archPath.CGPath, asOffset: false, orientToPath: true, duration: duration)
//        followPath.timingMode = .EaseIn
        let waitAction = SKAction.waitForDuration(NSTimeInterval(CGFloat(index) * 0.05))
        let fadeOut = SKAction.fadeOutWithDuration(0.5) //, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 0)
        fadeOut.timingMode = .EaseIn
        let scaleIn = SKAction.scaleTo(0, duration: 0.5) //, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 0)
        scaleIn.timingMode = .EaseIn
        let moveTo = SKAction.moveTo(CGPoint(x: x, y: y), duration: 0.5) //, delay: 0, usingSpringWithDamping: 0.2, initialSpringVelocity: 0)
        moveTo.timingMode = .EaseIn
        let action = SKAction.runBlock {
            closure()
        }
        
        return SKAction.sequence([waitAction, SKAction.group([fadeOut, scaleIn, moveTo]), action])
    }
}
