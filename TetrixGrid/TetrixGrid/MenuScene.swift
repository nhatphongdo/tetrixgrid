//
//  MenuScene.swift
//  TetrixGrid
//
//  Created by Do Nhat Phong on 1/19/15.
//  Copyright (c) 2015 VietDev Corporation. All rights reserved.
//

import SpriteKit
import StoreKit
import Social

class MenuScene: SKScene, SKStoreProductViewControllerDelegate {
    
    let AvatarSize: Int = 120
    
    var spriteBackground = SKSpriteNode(imageNamed: "Background")
    var spriteSingleNormalButton = SKSpriteNode(imageNamed: "Button_Normal")
    var spriteSingleTimedButton = SKSpriteNode(imageNamed: "Button_Locked_Timed")
    var spriteMultiplayerButton = SKSpriteNode(imageNamed: "Button_Multiplayer")
    var spriteLeaderboardButton = SKSpriteNode(imageNamed :"Button_Leaderboard")
    var spriteSettingsButton = SKSpriteNode(imageNamed: "Button_Settings")
    var spriteExitButton = SKSpriteNode(imageNamed: "Button_Quit")
    var spriteShareButton = SKSpriteNode(imageNamed: "Button_Share")
    var spriteRateButton = SKSpriteNode(imageNamed: "Button_Rate")
    var spriteCopyrightText = SKSpriteNode(imageNamed: "Text_Copyright")
    var spriteBackgroundSettings = SKSpriteNode(imageNamed: "Background_SettingsMenu")
    var spriteSoundButton = SKSpriteNode(imageNamed: "Button_Sound")
    var spriteMusicButton = SKSpriteNode(imageNamed: "Button_Music")
    var spriteInfoButton = SKSpriteNode(imageNamed: "Button_Info")
    var spriteSoundOffOverlay = SKSpriteNode(imageNamed: "Overlay_Off")
    var spriteMusicOffOverlay = SKSpriteNode(imageNamed: "Overlay_Off")
    var spriteBestScore = SKSpriteNode(imageNamed: "Text_Best")
    var spriteAvatar = SKSpriteNode()
    var spriteMaskAvatar = SKSpriteNode(imageNamed: "Mask_Avatar")
    var usernameLabel = SKLabelNode(fontNamed: "Avenir")
    var bestScoreLabel = SKLabelNode(fontNamed: "ChalkboardSE-Bold")
    
    var currentZoomingButton: SKSpriteNode? = nil
    
    var gameScene: GameScene? = nil
    
    var viewController: MenuViewController? = nil
    
    var loaded = false
    
    override func didMoveToView(view: SKView) {
        /* Setup your scene here */
        viewController?.showBottomAd()
        
        if loaded {
            bestScoreLabel.text = String(UserData.UserSettings.bestScore)
            if UserData.UserSettings.soundEnabled {
                spriteSoundOffOverlay.alpha = 0
            }
            else {
                spriteSoundOffOverlay.alpha = 1
            }
            if UserData.UserSettings.musicEnabled {
                spriteMusicOffOverlay.alpha = 0
            }
            else {
                spriteMusicOffOverlay.alpha = 1
            }
            
            return
        }
        
        let gap = CGFloat(20)
        var y = CGFloat(0)
        
        spriteBackground.position = CGPoint(x: size.width / 2, y: size.height / 2)
        self.addChild(spriteBackground)
        
        // Align 3 buttons in vertical middle
        y = (size.height - (spriteSingleNormalButton.size.height + spriteSingleTimedButton.size.height + spriteLeaderboardButton.size.height + 4 * gap)) / 2
        
        // Add leaderboard button
        y += spriteLeaderboardButton.size.height / 2
        spriteLeaderboardButton.position = CGPoint(x: size.width / 2, y: y)
        self.addChild(spriteLeaderboardButton)
        
        // Add single timed mode button
        y += spriteLeaderboardButton.size.height + 2 * gap
        spriteSingleTimedButton.position = CGPoint(x: size.width / 2 - 7 , y: y)
        self.addChild(spriteSingleTimedButton)
        
        // Add single normal mode button
        y += spriteSingleTimedButton.size.height + 2 * gap
        spriteSingleNormalButton.position = CGPoint(x: size.width / 2, y: y)
        self.addChild(spriteSingleNormalButton)
        
        // Add best score label
        y += spriteSingleNormalButton.size.height + 2 * gap + spriteBestScore.size.height + gap
        spriteBestScore.position = CGPoint(x: size.width / 2, y: y)
        self.addChild(spriteBestScore)
        
        // Add best score
        y -= (spriteBestScore.size.height)
        bestScoreLabel.fontSize = 72
        bestScoreLabel.text = String(UserData.UserSettings.bestScore)
        bestScoreLabel.fontColor = UIColor(red: 79/255.0, green: 150/255.0, blue: 45/255.0, alpha: 1)
        bestScoreLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Center
        bestScoreLabel.verticalAlignmentMode = SKLabelVerticalAlignmentMode.Top
        bestScoreLabel.position = CGPoint(x: size.width / 2, y: y)
        bestScoreLabel.zPosition = 90
        self.addChild(bestScoreLabel)
        
        // Add user's avatar
        var cropNode = SKCropNode()
        spriteAvatar.size = CGSize(width: AvatarSize, height: AvatarSize)
        spriteAvatar.position = CGPoint(x: gap + spriteAvatar.size.width / 2, y: size.height - gap - spriteAvatar.size.height / 2)
        spriteMaskAvatar.size = spriteAvatar.size
        spriteMaskAvatar.position = spriteAvatar.position
        cropNode.addChild(spriteAvatar)
        cropNode.maskNode = spriteMaskAvatar
        cropNode.zPosition = 99
        cropNode.position = CGPoint(x: 0, y: 0)
        self.addChild(cropNode)
        
        // Add username
        usernameLabel.text = ""
        usernameLabel.fontColor = UIColor(red: 0.0, green: 0.0, blue: 0.0, alpha: 1.0)
        usernameLabel.fontSize = 35
        usernameLabel.verticalAlignmentMode = SKLabelVerticalAlignmentMode.Center
        usernameLabel.horizontalAlignmentMode = SKLabelHorizontalAlignmentMode.Left
        usernameLabel.position = CGPoint(x: gap + spriteAvatar.size.width + gap, y: size.height - gap - spriteAvatar.size.height / 2)
        usernameLabel.zPosition = 90
        self.addChild(usernameLabel)
        
        // Create settings menu group
        spriteBackgroundSettings.anchorPoint = CGPoint(x: 0.5, y: 1)
        spriteBackgroundSettings.position = CGPoint(x: size.width - gap - spriteSettingsButton.size.width / 2, y: size.height - spriteAvatar.size.height / 2 + gap)
        self.addChild(spriteBackgroundSettings)
        
        // Add copyright text
        spriteCopyrightText.position = CGPoint(x: size.width / 2, y: gap + spriteCopyrightText.size.height / 2)
        self.addChild(spriteCopyrightText)
        
        // Add exit button
        spriteExitButton.position = CGPoint(x: gap + spriteExitButton.size.width / 2, y: gap + spriteExitButton.size.height / 2)
        //self.addChild(spriteExitButton)
        
        // Add share button
        spriteShareButton.position = CGPoint(x: (size.width - spriteLeaderboardButton.size.width) / 2 + spriteShareButton.size.width / 2, y: gap + spriteExitButton.size.height + 2 * gap + spriteShareButton.size.height / 2)
        self.addChild(spriteShareButton)
        
        // Add rate button
        spriteRateButton.position = CGPoint(x: (size.width + spriteLeaderboardButton.size.width) / 2 - spriteRateButton.size.width / 2, y: gap + spriteExitButton.size.height + 2 * gap + spriteRateButton.size.height / 2)
        self.addChild(spriteRateButton)
        
        // Add sound button
        y = -spriteSettingsButton.size.height - gap
        spriteSoundButton.anchorPoint = CGPoint(x: 0.5, y: 1)
        spriteSoundButton.position = CGPoint(x : 0, y: y)
        spriteBackgroundSettings.addChild(spriteSoundButton)
        
        // Add music button
        y -= spriteSoundButton.size.height + 2 * gap
        spriteMusicButton.anchorPoint = CGPoint(x: 0.5, y: 1)
        spriteMusicButton.position = CGPoint(x : 0, y: y)
        spriteBackgroundSettings.addChild(spriteMusicButton)
        
        // Add info button
        y -= spriteInfoButton.size.height + 2 * gap
        spriteInfoButton.anchorPoint = CGPoint(x: 0.5, y: 1)
        spriteInfoButton.position = CGPoint(x : 0, y: y)
        spriteBackgroundSettings.addChild(spriteInfoButton)
        
        // Add overlay
        spriteSoundOffOverlay.position = CGPoint(x: spriteSoundButton.position.x, y: spriteSoundButton.position.y - spriteSoundButton.size.height / 2)
        spriteBackgroundSettings.addChild(spriteSoundOffOverlay)
        spriteMusicOffOverlay.position = CGPoint(x: spriteMusicButton.position.x, y: spriteMusicButton.position.y - spriteMusicButton.size.height / 2)
        spriteBackgroundSettings.addChild(spriteMusicOffOverlay)
        
        // Add settings button
        spriteSettingsButton.position = CGPoint(x: size.width - gap - spriteSettingsButton.size.width / 2, y: size.height - gap - spriteAvatar.size.height / 2)
        self.addChild(spriteSettingsButton)
        
        // Set default state
        spriteBackgroundSettings.yScale = 0
        if UserData.UserSettings.soundEnabled {
            spriteSoundOffOverlay.alpha = 0
        }
        else {
            spriteSoundOffOverlay.alpha = 1
        }
        if UserData.UserSettings.musicEnabled {
            spriteMusicOffOverlay.alpha = 0
        }
        else {
            spriteMusicOffOverlay.alpha = 1
        }
        
        loaded = true
    }
    
    override func touchesBegan(touches: NSSet, withEvent event: UIEvent) {
        /* Called when a touch begins */
        
        for touch: AnyObject in touches {
            let location = touch.locationInNode(self)
            
            currentZoomingButton = nil
            let scale = SKAction.scaleTo(CGFloat(1.2), duration: 0.2)
            var action: SKAction = scale
            if UserData.UserSettings.soundEnabled {
                action = SKAction.group([scale, SoundLib.getSoundAction(SoundNames.Touch)])
            }
            
            if spriteSingleNormalButton.containsPoint(location) {
                spriteSingleNormalButton.runAction(action)
                currentZoomingButton = spriteSingleNormalButton
            }
            else if spriteSingleTimedButton.containsPoint(location) {
                spriteSingleTimedButton.runAction(action)
                currentZoomingButton = spriteSingleTimedButton
            }
            else if spriteLeaderboardButton.containsPoint(location) {
                spriteLeaderboardButton.runAction(action)
                currentZoomingButton = spriteLeaderboardButton
            }
            else if spriteSettingsButton.containsPoint(location) {
                spriteSettingsButton.runAction(action)
                currentZoomingButton = spriteSettingsButton
            }
            else if spriteShareButton.containsPoint(location) {
                spriteShareButton.runAction(action)
                currentZoomingButton = spriteShareButton
            }
            else if spriteRateButton.containsPoint(location) {
                spriteRateButton.runAction(action)
                currentZoomingButton = spriteRateButton
            }
            else {
                let settingsLocation = touch.locationInNode(spriteBackgroundSettings)
                
                if spriteBackgroundSettings.yScale == 1 && spriteMusicButton.containsPoint(settingsLocation) {
                    currentZoomingButton = spriteMusicButton
                }
                else if spriteBackgroundSettings.yScale == 1 && spriteSoundButton.containsPoint(settingsLocation) {
                    currentZoomingButton = spriteSoundButton
                }
                else if spriteBackgroundSettings.yScale == 1 && spriteInfoButton.containsPoint(settingsLocation) {
                    currentZoomingButton = spriteInfoButton
                }
            }
        }
    }
    
    override func touchesEnded(touches: NSSet, withEvent event: UIEvent) {
        /* Called when a touch ends */
        
        for touch: AnyObject in touches {
            let currentViewController: UIViewController = UIApplication.sharedApplication().keyWindow!.rootViewController!
            
            let action = SKAction.scaleTo(CGFloat(1), duration: 0.2)
            if (currentZoomingButton !== nil) {
                currentZoomingButton?.runAction(action)
            }
            
            if spriteSingleNormalButton === currentZoomingButton {
                // Move to game scene with normal mode
                let transition = SKTransition.doorwayWithDuration(0.5)
                gameScene!.Settings.Mode = GameModes.Normal
                self.view!.presentScene(gameScene, transition: transition)
            }
            else if spriteSingleTimedButton === currentZoomingButton {
                //                let transition = SKTransition.doorwayWithDuration(0.5)
                //                gameScene!.Settings.Mode = GameModes.Timed
                //                self.view!.presentScene(gameScene, transition: transition)
            }
            else if spriteLeaderboardButton === currentZoomingButton {
//                let transition = SKTransition.doorwayWithDuration(0.5)
//                let scene = LeaderboardScene(size: self.size)
//                scene.scaleMode = .AspectFit
//                scene.previousScene = self
//                self.view!.presentScene(scene, transition: transition)
//                
//                let settingsAction = SKAction.scaleYTo(CGFloat(0), duration: 0.3)
//                spriteBackgroundSettings.runAction(settingsAction)
                if UserData.UserSettings.gameCenter != nil {
                    UserData.UserSettings.gameCenter!.showLeaderboard(currentViewController)
                }
            }
            else if spriteSettingsButton === currentZoomingButton {
                // Scroll down settings menu
                if (spriteBackgroundSettings.yScale == 0) {
                    let settingsAction = SKAction.scaleYTo(CGFloat(1), duration: 0.3)
                    spriteBackgroundSettings.runAction(settingsAction)
                }
                else {
                    let settingsAction = SKAction.scaleYTo(CGFloat(0), duration: 0.3)
                    spriteBackgroundSettings.runAction(settingsAction)
                }
            }
            else if spriteShareButton === currentZoomingButton {
                shareToFacebook("Let's play TeBreak with me", image: UIImage(named: "Icon_Game"))
            }
            else if spriteRateButton === currentZoomingButton {
                rateApp()
            }
            else if spriteMusicButton === currentZoomingButton {
                if UserData.UserSettings.musicEnabled {
                    UserData.UserSettings.musicEnabled = false
                    spriteMusicOffOverlay.alpha = 1
                }
                else {
                    UserData.UserSettings.musicEnabled = true
                    spriteMusicOffOverlay.alpha = 0
                }
                UserData.setUserData("MusicEnabled", value: UserData.UserSettings.musicEnabled)
            }
            else if spriteSoundButton === currentZoomingButton {
                if UserData.UserSettings.soundEnabled {
                    UserData.UserSettings.soundEnabled = false
                    spriteSoundOffOverlay.alpha = 1
                }
                else {
                    UserData.UserSettings.soundEnabled = true
                    spriteSoundOffOverlay.alpha = 0
                }
                UserData.setUserData("SoundEnabled", value: UserData.UserSettings.soundEnabled)
            }
            else if spriteInfoButton === currentZoomingButton {
                let transition = SKTransition.doorwayWithDuration(0.5)
                let scene = GuideScene(size: self.size)
                scene.scaleMode = .AspectFit
                scene.previousScene = self
                scene.viewController = self.viewController
                self.view!.presentScene(scene, transition: transition)
                
                let settingsAction = SKAction.scaleYTo(CGFloat(0), duration: 0.3)
                spriteBackgroundSettings.runAction(settingsAction)
            }
            
            currentZoomingButton = nil
        }
    }
    
    override func update(currentTime: CFTimeInterval) {
        /* Called before each frame is rendered */
    }
    
    func setUsername(name: String) {
        usernameLabel.text = name
    }
    
    func rateApp() {
        let appId = "963058461"
        
        // Use the in-app StoreKit view if available (iOS 6) and imported. This works in the simulator.
        if NSStringFromClass(SKStoreProductViewController.self) != nil {
            let storeViewController = SKStoreProductViewController()
            storeViewController.loadProductWithParameters([SKStoreProductParameterITunesItemIdentifier: appId], completionBlock: { (value, error) -> Void in
                
            })
            storeViewController.delegate = self
            
            let currentViewController: UIViewController = UIApplication.sharedApplication().keyWindow!.rootViewController!
            currentViewController.presentViewController(storeViewController, animated: true, completion: { () -> Void in
            })
        }
        else {
            // Use the standard openUrl method if StoreKit is unavailable.
            let templateReviewURL = "itms-apps://ax.itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?type=Purple+Software&id=APP_ID"
            let templateReviewURLiOS7 = "itms-apps://itunes.apple.com/app/idAPP_ID"
            let templateReviewURLiOS8 = "itms-apps://itunes.apple.com/WebObjects/MZStore.woa/wa/viewContentsUserReviews?id=APP_ID&onlyLatestVersion=true&pageNumber=0&sortOrdering=1&type=Purple+Software"
            var reviewURL = templateReviewURL.stringByReplacingOccurrencesOfString("APP_ID", withString: appId)
            
            if (UIDevice.currentDevice().systemVersion as NSString).floatValue >= 7.0 && (UIDevice.currentDevice().systemVersion as NSString).floatValue < 8.0 {
                reviewURL = templateReviewURLiOS7.stringByReplacingOccurrencesOfString("APP_ID", withString: appId)
            }
            else if (UIDevice.currentDevice().systemVersion as NSString).floatValue >= 8.0 {
                reviewURL = templateReviewURLiOS8.stringByReplacingOccurrencesOfString("APP_ID", withString: appId)
            }
            
            UIApplication.sharedApplication().openURL(NSURL(string: reviewURL)!)
        }
    }
    
    func productViewControllerDidFinish(viewController: SKStoreProductViewController!) {
        viewController.dismissViewControllerAnimated(true, completion: { () -> Void in
        })
    }
    
    func shareToFacebook(text: String, image: UIImage?) {
        let currentViewController: UIViewController = UIApplication.sharedApplication().keyWindow!.rootViewController!
        if SLComposeViewController.isAvailableForServiceType(SLServiceTypeFacebook){
            var facebookSheet:SLComposeViewController = SLComposeViewController(forServiceType: SLServiceTypeFacebook)
            facebookSheet.setInitialText(text)
            facebookSheet.addImage(image)
            currentViewController.presentViewController(facebookSheet, animated: true, completion: nil)
        } else {
            var alert = UIAlertController(title: "TeBreak", message: "Please login to a Facebook account to share.", preferredStyle: UIAlertControllerStyle.Alert)
            alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
            currentViewController.presentViewController(alert, animated: true, completion: nil)
        }
    }
}
